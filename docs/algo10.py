def suitearithm(u,r,N):
    """
    u est le premier terme
    r est la raison
    N est le nombre de termes
    """
    #création d'une liste vide
    L=[]
    for i in range(N):
        L.append(u)
        u=u+r
    return L

assert suitearithm(-4,1,10) == [-4, -3, -2, -1, 0, 1, 2, 3, 4, 5]
assert suitearithm(5.7,-0.5,8) == [5.7, 5.2, 4.7, 4.2, 3.7, 3.2, 2.7, 2.2]