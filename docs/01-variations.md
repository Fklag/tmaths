<!-- # Rappels sur les suites numériques de première générale -->

<!-- ## Variation d'une suite -->

!!! tip "Définition ▶ Variations"
    • Une suite $(u_n)_{n \geqslant n_0}$ est **croissante** (respectivement **strictement croissante**) si,  
      pour tout $n \geqslant n_0, \quad u_n \leqslant u_{n+1}$ (respectivement $u_n < u_{n+1}$ ).  

    • Une suite $(u_n)_{n \geqslant n_0}$ est **décroissante** (respectivement **strictement décroissante**) si,  
      pour tout $n \geqslant n_0, \quad u_n \geqslant u_{n+1}$ (respectivement $u_n > u_{n+1}$ ).  
      
    • Une suite qui est croissante (respectivement **strictement croissante**) ou décroissante (respectivement **strictement décroissante**) est dite **monotone (respectivement strictement monotone)**.

!!! abstract "Propriété ▶ Méthodes pour déterminer le sens de variation d'une suite"
    Soit une suite $(u_n)$ définie à partir d'un certain rang $n_0$ (en général $n_0 = 0$ ou $n_0 = 1$).

    1. L'étude du **signe de $u_{n+1} - u_n$** renseigne sur les variations de la suite $(u_n)$ :  
      • si, pour tout $n \geqslant n_0 , \quad u_{n+1} - u_n \geqslant 0$, la suite **$(u_n)$ est croissante**.    
      • si, pour tout $n \geqslant n_0 , \quad u_{n+1} - u_n \leqslant 0$, la suite **$(u_n)$ est décroissante**.  

    2. Si la suite est définie par une relation explicite, on peut étudier le sens de variation de la fonction associée.

??? faq "À titre d'exemple"
    === "Exemple 3 : "
        Soit $(u_n)$ la suite définie par : $\begin{cases}
        u_0 & = & 4  \\
        u_{n+1} & = & -3n + 1 + u_n  \quad \forall n \in \mathbb{N}\\
        \end{cases}$

        1. Calculer $u_1$, $u_2$ et $u_3$ .

        2. Écrire dans le cours le code `Python` à compléter de manière ce qu'il retourne le terme $u_n$, pour tout entier $n$.  
           Vérifier vos calculs précédents et donner la valeur de $u_{50}$ .

        3. Étudier les variations de $(u_n)$.

          ```py
          def terme_rang_n(N):
            pass

          assert terme_rang_n(0) == 4
          assert terme_rang_n(1) == 5
          ```
        

          <!-- {{ IDE('exemple3') }} -->

        ??? bug solution "Réponse"
            - $u_0 = 4$
            - $u_1 = u_{0 +1} = -3 \times 0 + 1 + u_0 = 0 + 1 + 4 = 5$
            - $u_2 = u_{1 +1} = -3 \times 1 + 1 + u_1 = -3 + 1 + 5 = 3$
            - $u_3 = u_{2 +1} = -3 \times 2 + 1 + u_2 = -6 + 1 + 3 = -2$

            ```py
              def terme_rang_n(N):
                u=4
                for i in range(N):
                  u=-3*i+1+u
                return u

              assert terme_rang_n(0) == 4
              assert terme_rang_n(1) == 5
              assert terme_rang_n(2) == 3
              assert terme_rang_n(3) == -2
              assert terme_rang_n(50) == -3621
            ```
            Étudions le signe de $u_{n+1} - u_n$.  

            $$
            \begin{eqnarray*}
            u_{n+1} - u_n &=& \underbrace{-3n + 1 + u_n}_{u_{n+1}} - u_n \\
                          &=& -3n + 1 \\
            \end{eqnarray*}
            $$

            Donc $u_{n+1} - u_n < 0$, dès que $n > \frac{1}{3}$, soit dès que $n \geqslant 1$.  
            La suite $(u_n)$ est donc strictement décroissante à partir du rang $1$.

        <!-- 3. Étudier les variations de $(u_n)$. -->


    === "Exemple 4 : "
        Soit $(v_n)$ la suite définie par : $v_n = 5n^2 + 9n + 3$

        1. Calculer $v_0$, $v_1$ et $v_2$ .

        2. Écrire dans le cours le code `Python` à compléter de façon à obtenir la liste des $N$ premiers termes de la suite.  
           Exécuter ce programme pour $N=10$.

        3. Étudier les variations de $(v_n)$.

          ```py
          def f(x):
            pass

          def suite_explicite(N):
            pass

          assert f(3) == 75
          assert suite_explicite(10) == [3, 17, 41, 75, 119, 173, 237, 311, 395, 489]
          ```
        

          <!-- {{ IDE('exemple4') }} -->

        ??? bug solution "Réponse"
            Pour tout entier naturel $n, \, v_n = f(n)$, où $f$ est la fonction définie par $f(x) = 5x^2 + 9x + 3$.  
            La suite $(v_n)$ est définie de manière explicite.

            - $v_0 = f(0) = 5 \times 0^2 + 9\times 0 + 3 = 3$
            - $v_1 = f(1) = 5 \times 1^2 + 9\times 1 + 3 = 17$
            - $v_2 = f(2) = 5 \times 2^2 + 9\times 2 + 3 = 41$

            ```py
              def f(x):
                return 5*x**2+9*x+3

              def suite_explicite(N):
                L=[]
                for i in range(N):
                  L.append(f(i))
                return L

              assert f(3) == 75
              assert suite_explicite(10) == [3, 17, 41, 75, 119, 173, 237, 311, 395, 489]
            ```

            La fonction $f : x \mapsto 5x^2 + 9x + 3$ est définie et dérivable sur $\mathbb{R}$.  
            Pour tout réel $x, \, f'(x) = 10x + 9$. $\quad f'(x) > 0$ si, et seulement si $x > -\dfrac{9}{10}$.  
            La fonction $f$ est donc strictement décroissante sur $\left]-\infty~;~-\frac{9}{10}\right]$ et strictement croissante sur $\left[-\frac{9}{10}~;~+\infty\right[$. Soit $n \in \mathbb{N}$ 

            $$
            \begin{eqnarray*}
            0 &\leqslant& n &<& n+1 & \\
              & & f(n) &<& f(n + 1) & \quad \text{car la fonction est strictement croissante sur } \left[0~;~+\infty\right[ \\
              & & v_n  &<& v_{n+1} & \\
              & & 0 &<& v_{n+1} - v_n  &
            \end{eqnarray*}
            $$

            $\forall n \in \mathbb{N}, \, v_{n+1} - v_n > 0$, donc la suite $(v_n)_{n\in\mathbb{N}}$ est strictement croissante.

{{ IDE('exemple3') }}