def sommegeom(u,q,N):
    """
    u est le premier terme
    q est la raison
    N est le nombre de termes
    """
    #création d'une liste vide
    S = 0
    for i in range(N):
        S = S + u
        u= q*u
    return S

# sommegeom(128,0.25,25) -> 170.6666666666666
# sommegeom(128*0.25**13,0.25,39) -> 2.5431315104166665e-06