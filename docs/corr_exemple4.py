def f(x):
    return 5*x**2+9*x+3

def suite_explicite(N):
    L=[]
    for i in range(N):
        L.append(f(i))
    return L

assert f(3) == 75
assert suite_explicite(10) == [3, 17, 41, 75, 119, 173, 237, 311, 395, 489]