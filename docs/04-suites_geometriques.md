<!-- # Rappels sur les suites numériques de première générale -->

<!-- ## Suites géométriques -->

## Exemples
??? faq "Exemples"
    === "Exemple 17 : "
        On commence à $2$ et on passe d'un terme au suivant en multipliant par la même valeur $3$.

        $$\left\{2 \, ; \, 6 \, ; \, 18 \, ; \, 54 \, ;\, 162 \, ; \, 486 \, ;\, 1458 \, ;\, 4374 \, ;\, \ldots \right\}$$   

    === "Exemple 18 : "
        On commence à $5$ et on passe d'un terme au suivant en multipliant par la même valeur $\frac{1}{2}$.

        $$\left\{5 \, ; \, 2,5 \, ; \, 1,25 \, ; \, 0,625 \, ;\, 0,3125 \, ; \, 0,15625 \, ;\, 0,078125 \, ;\, 0,0390625 \, ;\, \ldots \right\}$$  

## Définition par récurrence

!!! tip "Définition ▶ relation de récurrence"
    Une suite est **géométrique** si chaque terme se déduit du précédent en le multipliant par un nombre constant, appelé raison qui est notée $q$.  
    On commence au rang $n_0$ . Notons $a$ la valeur du premier terme.

    $$
    \begin{cases}
    u_{n_0} & = & a \\
    u_{n+1} & = & u_n \times q \quad \forall n \in \mathbb{N}, n \geqslant n_0\\
    \end{cases}
    $$

    Par exemple si le premier terme est $u_0$ , on obtient :

    ![geometrique](images/geomet.png)

!!! tip "Définition ▶ relation de récurrence et relation fonctionnelle associée"
    La relation de récurrence d'une suite agéométrique peut s'exprimer à l'aide d'une **fonction linéaire**.  
    Cette fonction permet le passage d'un terme de la suite au terme suivant.

    $$
    \begin{cases}
    u_{n_0} & = & a \\
    u_{n+1} & = & f(u_n) \quad \forall n \in \mathbb{N}, n \geqslant n_0 \, \text{ où } \, f : x \mapsto q \times x \\
    \end{cases}
    $$

!!! note "Exercice 16: Recherche"
    En reprenant les deux exemples précédents, déterminer le premier terme de chaque suite, la relation de récurrence, puis la fonction $f$ qui leur est associée.

    === "Exemple 17: "
        ??? bug solution "Réponse"
            - $u_0 = 2$
            - $u_{n+1} = u_{n} \times 3$
            - $f : x \mapsto 3 \times x$


    === "Exemple 18: "
        ??? bug solution "Réponse"
            - $u_0 = 5$
            - $u_{n+1} = u_{n} \times \frac{1}{2}$
            - $f : x \mapsto \frac{1}{2} \times x$

!!! note "Développer les automatismes avec Labomep/wim's"
    === "Exercice 17: "
        [Identifier une suite géométrique](https://bibliotheque.sesamath.net/public/voir/91382){target=_blank}

??? faq "À titre d'exemple ▶ Générer une suite avec GeoGebra"
    === "Exemple 19 : "
        La suite géométrique est définie par la relation de récurrence $\begin{cases}
        u_{0} & = & 2 \\
        u_{n+1} & = & 3 u_n \quad \forall n \in \mathbb{N} \\
        \end{cases}$

        ![exemple19](images/exemple19.png)

    === "Exemple 20 : "
        La suite géométrique est définie par la relation de récurrence $\begin{cases}
        u_{1} & = & 5 \\
        u_{n+1} & = & 0,5 u_n \quad \forall n \in \mathbb{N} \\
        \end{cases}$

        ![exemple20](images/exemple20.png)

??? faq "À titre d'exemple ▶ Génération d'une suite avec un tableur"
    On utilise le tableur de GeoGebra pour d'une part générer les éléments de la suite de l'exemple 19 qui est définie par récurrence, puis pour représenter graphiquement la suite par un nuage de points.
    === "Exemple 21 : "
        Voir la vidéo  
        <iframe width="1280" height="676" src="https://www.youtube.com/embed/3Vm0WCsCGFc" title="Suite, tableur, et nuage de points" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    === "Exemple 22 : "
        En vous inspirant de la vidéo précédente, générer les éléments de la suite de l'exemple 20, puis représenter graphiquement la suite par un nuage de points.

??? danger "Algorithme et Python ▶ Génération d'une suite géométrique"
    On souhaite générer un nombre entier N de valeurs d'une suite géométrique définie par récurrence.
    === "Algorithme : "
        On saisit le premier terme $u$.  
        On saisit la raison $q$.  
        On saisit $N$.  
        $L$ est une liste vide  
        Pour $i$ allant de $1$ à $N$  
        $\, \, \, \,$ On ajoute $u$ dans la liste $L$  
        $\, \, \, \,$ $u \longleftarrow q * u$  
        Fin du Pour
     
    === "Programme en `Python`: "
        ```py
        def suitegeom(u,q,N):
            """
            u est le premier terme
            q est la raison
            N est le nombre de termes
            """
            #création d'une liste vide
            L=[]
            for i in range(N):
                L.append(u)
                u=q*u
            return L

        assert suitegeom(2,3,10) == [2, 6, 18, 54, 162, 486, 1458, 4374, 13122, 39366]
        assert suitegeom(5,0.5,9) == [5, 2.5, 1.25, 0.625, 0.3125, 0.15625, 0.078125, 0.0390625, 0.01953125]
        ```

## Augmentation et diminution exprimées en pourcentage

!!! abstract "Propriété ▶ Coefficient multiplicateur"
    Rappels :  
    • Augmenter une grandeur de $p \%$ revient à la multiplier par $1 + \frac{p}{100}$ .  
    • Diminuer une grandeur de $p \%$ revient à la multiplier par $1 - \frac{p}{100}$ .  

    Une succession d'augmentations (resp. de % diminutions) d'un même pourcentage p % peut être modélisée par une suite géométrique de raison 
    
    $$\boxed{\textcolor{red}{1 + \frac{p}{100} \quad (\text{ resp. } 1 - \frac{p}{100} ) }}$$

    ??? faq "Exemples"
        === "Par exemple : "
            Si les émissions de $\text{CO}_2$ augmentent chaque année de $3 \%$ (le coefficient multiplicateur est alors égal à $1 + \frac{3}{100} = 1,03$) on peut modéliser la masse des émissions de l'année $n$ par la suite $u_n$ définie par la relation : $u_{n+1} = u_n \times 1,03$.  
            La suite $(u_n)$ est géométrique de raison $1,03$.

        === "Par exemple : "
            Un T-shirt soldé voit son prix diminuer chaque jour de $2,5 \%$, ce qui correspond à un coefficient multiplicateur égal à $1 - \frac{2,5}{100} = 0,975$  
            en notant $v_n$ le prix du T-shirt au bout de $n$ jours de soldes, on a la relation : $v_{n+1} = v_n \times 0,975$.  
            La suite $(u_n)$ est géométrique de raison $1,03$.

!!! note "Exercice: Recherche"
    === "Exercice 18: "
        Une société de vente en ligne a vendu $10\ 000$ consoles de jeux vidéo en 2021. En se basant sur les évolutions du marché, les responsables font l'hypothèse qu'à partir de l'année 2021, les ventes vont diminuer de $4 \%$ par an.  
        On note $v_n$ le nombre de consoles vendues par cette société lors de l'année $2021 + n$.  
        Exprimer $v_{n+1}$ en fonction de $v_n$ ; que peut-on dire de cette suite ?


## Relation explicite

!!! abstract "Propriété ▶ relation explicite"
    Relation explicite pour une suite géométrique : soit $(u_n)_{n\geqslant 0}$ une suite géométrique de raison $q$.  
        - $\forall n \in \mathbb{N}, u_n = u_0 \times q^n$ .  
        - $\forall n \in \mathbb{N}, u_n = u_1 \times q^{n-1}$ .  
        - $\forall n \in \mathbb{N}, u_n = u_2 \times q^{n-2}$ .  
        - etc $\ldots$

    Plus généralement, on a la formule importante suivante :

    $$\boxed{\textcolor{red}{\forall n \in \mathbb{N}, \forall p \in \mathbb{N}, u_n = u_p \times q^{n-p}}}$$

    La forme explicite permet l'obtention rapide des termes de chaque rang de la suite $(u_n)$ .

??? faq "À titre d'exemple"
    === "Exemple 23 : "
        La suite géométrique est définie par la relation de récurrence $\begin{cases}
        u_0 & = & 2 \\
        u_{n+1} & = & 3 \times u_{n} \quad \forall n \in \mathbb{N}
        \end{cases}$

        Sa forme explicite est donc :

        $$
        \begin{eqnarray*}
        u_n &=& \underbrace{u_0}_{2} \times \underbrace{q^n}_{3^n}\\
            &=& 2 \times 3^n \\
        \end{eqnarray*}
        $$

        Calculons par exemple le 5ème terme de la suite $(u_n)$. Comme le premier terme est $u_0$ , alors le le 5ème terme est $u_{4}$ .

        $$\boxed{\textcolor{red}{u_{4} = 2 \times 3^4 = 162}}$$

    === "Exemple 24 : "
        La suite géométrique est définie par la relation de récurrence $\begin{cases}
        u_1 & = & 5 \\
        u_{n+1} & = & 0,5 u_{n} \quad \forall n \in \mathbb{N}
        \end{cases}$

        Sa forme explicite est donc :

        $$
        \begin{eqnarray*}
        u_n &=& \underbrace{u_1}_{5} \times \underbrace{q^{n-1}}_{0,5^{n-1}}\\
            &=& 5 \times 0,5^{n-1}
        \end{eqnarray*}
        $$

        Calculons par exemple le 7ème terme de la suite $(u_n)$. Comme le premier terme est $u_1$ , alors le le 7ème terme est $u_{7}$ .

        $$\boxed{\textcolor{red}{u_{7} = 5 \times 0,5^6 = 0,078125}}$$

!!! note "Développer les automatismes avec Labomep/wim's ▶ Suites géométriques"
    === "Exercice 19: "
        [Nature, terme général et calcul d'un terme d'une suite géométrique](https://bibliotheque.sesamath.net/public/voir/91950){target=_blank}

    === "Exercice 20: "
        [Terme général d'une suite géométrique](https://bibliotheque.sesamath.net/public/voir/91384){target=_blank}      

!!! note "Exercice: Recherche"
    === "Exercice 21: "
        Savoir déterminer la raison et le premier terme de la suite géométrique connaissant deux de ses éléments et leur rang associé.

        Déterminer la raison et le premier terme de la suite géométrique $(u_n)_{n\in \mathbb{N}}$ telle que $\begin{cases}
        u_7 & = & 16 \\
        u_4 & = & 2
        \end{cases}$

        ??? bug solution "Réponse"
            Pour vous aider, une vidéo d'Yvan Monka.  

            <iframe width="1280" height="720" src="https://www.youtube.com/embed/wUfleWpRr10" title="Déterminer une suite géométrique - Première" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Représentation graphique

!!! abstract "Propriété"
    === "$u_0 = a > 0 \, \text{ et } \, q > 1$ : croissance exponentielle "
        ![croiexp](images/croiexp.png){width=40%}
        La suite géométrique de premier terme $u_0 = a > 0$ et de raison $q > 1$ est croissante ; cela signifie que chaque terme est plus grand que le précédent.  
        On passe d'un terme au suivant en multipliant par le même nombre $q > 1$ : ce phénomène est qualifié de **croissance exponentielle**.
        

    === "$u_0 = a > 0 \, \text{ et } \, 0 < q < 1$ : décroissance exponentielle "
    ![decroiexp](images/decroiexp.png){width=40%}
        La suite géométrique de premier terme $u_0 = a > 0$ et de raison $0 < q < 1$ est décroissante ; cela signifie que chaque terme est plus petit que le précédent.  
        On passe d'un terme au suivant en multipliant par le même nombre $q$ compris entre $0$ et $1$ : ce phénomène est qualifié de **décroissance exponentielle**.
        

### À partir de la relation de récurrence

??? faq "À titre d'exemple ▶ Suite géométrique définie par récurrence et $0 < q < 1$"
    === "Exemple 25 :"
        Soit $(u_n)_{n\geqslant 0}$ la suite géométrique définie par $\begin{cases}
        u_0 & = & 5 \\
        u_{n+1} & = & 0,5 u_n \quad \forall n \in \mathbb{N}\\
        \end{cases}$

        Pour effectuer différentes simulations suivre [le lien](https://www.geogebra.org/m/w5t5ZQPY){target=_blank} ;  

        Les éléments de la suite apparaissent sur l'axe des abscisses.

        <iframe src="https://www.geogebra.org/classic/w5t5ZQPY?embed" width="1000" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius:
        4px;" frameborder="0"></iframe>

        ??? bug solution "Réponse"
            ![exemple25](images/exemple25.png)

??? faq "À titre d'exemple ▶ Suite géométrique définie par récurrence et $q > 1$"
    === "Exemple 26 :"
        Soit $(u_n)_{n\geqslant 0}$ la suite géométrique définie par $\begin{cases}
        u_0 & = & 2 \\
        u_{n+1} & = & 3 u_n \quad \forall n \in \mathbb{N}\\
        \end{cases}$

        Pour effectuer différentes simulations suivre [le lien](https://www.geogebra.org/m/w5t5ZQPY){target=_blank} ;  

        Les éléments de la suite apparaissent sur l'axe des abscisses.

        <iframe src="https://www.geogebra.org/classic/w5t5ZQPY?embed" width="1000" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius:
        4px;" frameborder="0"></iframe>

        ??? bug solution "Réponse"
            ![exemple26](images/exemple26.png)

### À partir de la relation explicite

??? faq "À titre d'exemple ▶ Avec Geogebra"
    === "Exercice 22 :"
        ![exercice22fig](images/exercice22fig.png){align=right width=40%} 

        Reprenons la suite définie dans l'exemple 26.  
        Dans cet exemple, la forme explicite du terme général est donnée par : 
        
        $$\forall n \in \mathbb{N}, u_n = 2 \times 3^n$$

        Dans la ligne de saisie on écrit : $L = \text{séquence}((k, 2∗ 3^k), 0, 7)$ .  
        On obtient alors une représentation graphique des éléments de la suite avec l'ensemble des 8 points d'abscisse $k$ et d'ordonnée $u_k$ pour $k$ entier, $k \in \left[ 0~;~7\right]$.

        Mais si dans la ligne de saisie on écrit : $L = \text{séquence}(2∗ 3^k , k, 0, 7)$ , on obtient les 8 premiers éléments de la suite  :  
        $\boxed{\textcolor{red}{ \text{liste1} = \left\{2, 6, 18, 54, 162, 486, 1\ 458, 4\ 374\right\}}}$

## Sens de variation d'une suite géométrique

!!! note "Recherche: "
    === "Exercice :"
        Soit $(u_n)$ une suite géométrique de premier terme $u_0 = 1$ et de raison $q$, avec $q \neq 0$. Alors :

        $$\forall n \in \mathbb{N}, \, u_n = q^n$$
        
        Observer les variations de la suite $(q^n)$ [en faisant varier la raison $q$ sur geogebra](https://www.geogebra.org/m/vgxpjmd7){target=_blank}.

        ??? bug solution "Réponse"
            <iframe src="https://www.geogebra.org/classic/vgxpjmd7?embed" width="1000" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius:
            4px;" frameborder="0"></iframe>

!!! abstract "Théorème ▶ Variations de $q^n$"
    Soit $(u_n)$ une suite géométrique de premier terme $u_0 = 1$ et de raison $q$, avec $q \neq 0$. Alors :  

    $$\forall n \in \mathbb{N}, \, u_n = q^n$$

    - Si $q < 0$, la suite $\left(q^n\right)$ n'est ni croissante, ni décroissante. On dit que la suite est alternée (le terme suivant un terme positif est négatif, et le terme suivant un terme négatif est positif).  
    - Si $0 < q < 1$, la suite $\left(q^n\right)$ est strictement décroissante.  
    - Si $q = 1$, la suite $\left(q^n\right)$ est constante et est toujours égale à son premier terme.  
    - Si $q > 1$, la suite $\left(q^n\right)$ est strictement croissante.

!!! abstract "Théorème ▶ Variations de $u_k q^n$"
    Soit $k \in \mathbb{N}$. Soit $(u_n)$ une suite géométrique de premier terme $u_k$ et de raison $q$ . 

    $$\forall n \in \mathbb{N}, n \geqslant k \, u_n = u_k q^{n-k}$$

    • **1er cas :** $u_k = 0$, alors la suite est identiquement nulle, c'est-à-dire que : $\forall n \in \mathbb{N}, n \geqslant k \quad u_n = 0$ .  
    • **2ème cas :** $u_k > 0$  
        ◦ Si $q < 0$ alors la suite $(u_ n)$ n'est ni croissante, ni décroissante. On dit qu'elle est alternée.  
        ◦ Si $0 < q < 1$, la suite $(u_ n)$ est strictement décroissante.  
        ◦ Si $q = 1$, la suite $(u_ n)$ est constante et est égal à son premier terme.  
        ◦ Si $q > 1$, la suite $(u_ n)$ est strictement croissante.  
    • **3ème cas :** $u_k < 0$  
        ◦ Si q < 0 alors la suite $(u_ n)$ n'est ni croissante, ni décroissante. On dit qu'elle est alternée.  
        ◦ Si $0 < q < 1$, la suite $(u_ n)$ est strictement croissante.  
        ◦ Si $q = 1$, la suite $(u_ n)$ est constante et est égal à son premier terme.  
        ◦ Si $q > 1$, la suite $(u_ n)$ est strictement décroissante.  

    ??? bug solution "Explication"
        Vous trouverez , [ici, une vidéo explicative](https://www.youtube.com/watch?v=pZVHXA8cxP8){target=_blank}

        <iframe width="1280" height="720" src="https://www.youtube.com/embed/pZVHXA8cxP8" title="Déterminer une suite géométrique - Première" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

!!! note "Exercice: Recherche"
    === "Exercice 24: "
        Déterminer le sens de variation des suites définies ci-dessous.

        - $(u_n)_{n\geqslant 0}$  géométrique de premier terme $1,5$ et de raison $3$.  
        - $(u_n)_{n\geqslant 0}$  géométrique de premier terme $1,5$ et de raison $-2$.  
        - $(u_n)_{n\geqslant 0}$  géométrique de premier terme $-2$ et de raison $1,5$.  

## Somme finie des éléments d'une suite géométrique

!!! abstract "Propriété ▶ Somme finie"
    La somme de plusieurs termes consécutifs d'une suite géométrique non constante (c'est-à-dire de raison $q \neq 1$)est donnée par la formule :

    $$\boxed{\textcolor{red}{S = (\text{ premier terme }) \times \left( \dfrac{1 - q^{\text{ nombres de termes }}}{1 - q}\right)}}$$

!!! abstract "Corollaire"
    Pour tout entier naturel $n$ et pour tout réel $q \neq 1$, on a :

    $$\boxed{\textcolor{red}{S = 1 + q + q^2 + q^3 + \ldots + q^n = \dfrac{1 - q^{n+1}}{1 - q}}}$$

    On reconnaît une suite géométrique de premier terme $1$ et de raison $q$.

??? faq "À titre d'exemple"
    === "Exemple 27 :"
        Soit $(u_n)_{n\geqslant 0}$ la suite géométrique de premier terme $128$ et de raison $\frac{1}{4}$. On a alors :

        $u_1 = 32~;~u_2 = 8~;~u_3 = 2~;~u_4 = \frac{1}{2}~;~u_5 = \frac{1}{8}~;~u_6 = \frac{1}{32}$ ; etc.

        **Forme explicite :**

        $\forall n \in \mathbb{N},  \, u_n = u_0 q^n = 128 \times \left(\frac{1}{4}\right)^n = \frac{128}{4^n}$ .

        Rappel sur la notation ”somme” : $\, S = u_0 + u_1 + u_2 + \ldots + u_n = \displaystyle \sum_{i=0}^{n} u_i$

        On souhaite calculer $\, \displaystyle \sum_{i=0}^{24} u_i$

        ◦ C'est la somme des 25 premiers termes ;  
        ◦ le premier terme est $u_0 = 128$ .  

        $$\sum_{i=0}^{24} u_i = 128 \times \dfrac{1 - \left(\frac{1}{4}\right)^{25}}{1 - \frac{1}{4}} = 128 \times \dfrac{1 - \left(\frac{1}{4}\right)^{25}}{\frac{3}{4}} \simeq 170,67$$

        On souhaite calculer $\, \displaystyle \sum_{i=13}^{51} u_i$

        ??? bug solution "Réponse"
            ◦ C'est la somme de $39$ termes ;  
            ◦ le premier terme est $u_{13} = 128 \times \left(\frac{1}{4}\right)^{13}$  

            $$\sum_{i=13}^{51} u_i =  128 \times \left(\frac{1}{4}\right)^{13} \times \dfrac{1 - \left(\frac{1}{4}\right)^{39}}{\frac{3}{4}} = 2,5 \times 10^{-6}$$ 

!!! note "Développer les automatismes avec Labomep/wim's"
    === "Exercice 25: "
        [Somme des termes d'une suite géométrique](https://bibliotheque.sesamath.net/public/voir/91388){target=_blank}

!!! note "Exercices: Recherche"
    === "Exercice 26: "
        Déterminer $S = \displaystyle \sum_{i=1}^{5} 3^i$ . Pour vous aider, vous pouvez suivre [ce lien](https://www.youtube.com/watch?v=gUkOjvAiZGA){target=_blank} .

    === "Exercice 27: "
        Soit $(v_n)_{n \geqslant 0}$ la suite géométrique de premier terme $18$ et de raison $1,1$ .
        Donner sa relation explicite, puis calculer$S = \displaystyle \sum_{i=5}^{30} v_i$ . 

??? danger "Algorithme et Python ▶ Somme finie des termes d'une suite géométrique"
    On souhaite effectuer la somme de $N$ termes d'une suite géométrique de raison $q$ et de premier terme $u_k$ .  
    On renseigne le premier terme $u_k$ , la raison $q$ et le nombre de termes $N$ consécutifs à sommer.
    === "Algorithme : "
        $u$ est le premier terme  
        $S \longleftarrow 0$  
        Pour $i$ allant de $0$ à $N-1$  
        $\, \, \, \,$ $S \longleftarrow S + u$  
        $\, \, \, \,$ $u \longleftarrow q \times u$  
        Fin du Pour
     
    === "Programme en `Python`: "
        ```py
        def sommegeom(u,q,N):
            """
            u est le premier terme
            q est la raison
            N est le nombre de termes
            """
            #création d'une liste vide
            S = 0
            for i in range(N):
                S = S + u
                u= q*u
            return S

        # sommegeom(128,0.25,25) -> 170.6666666666666
        # sommegeom(128*0.25**13,0.25,39) -> 2.5431315104166665e-06
        ```

## Exercices

!!! note "Recherche"
    === "Exercice 27: "
        Les suites $(u_n)_{n\in \mathbb{N}}$ , $(v_n)_{n\in \mathbb{N}}$, $(w_n)_{n\in \mathbb{N}}$ et $(y_n)_{n\in \mathbb{N}}$ sont définies ci-dessous.  
        
        Pour chacune d'elles, préciser s'il s'agit d'une suite arithmétique ou géométrique, et préciser éventuellement sa raison.

        $$\begin{cases}
        u_0 & = & -3 \\
        u_{n+1} & = & u_{n} -4\quad \forall n \in \mathbb{N}
        \end{cases}$$
        
        $$\begin{cases}
        v_0 & = & 3 \\
        v_{n+1} & = & v_{n} + n \quad \forall n \in \mathbb{N}
        \end{cases}$$

        $$\begin{cases}
        w_0 & = & 3 \\
        w_{n+1} & = & 5 w_{n} \quad \forall n \in \mathbb{N}
        \end{cases}$$

        $$\begin{cases}
        y_0 & = & 1 \\
        y_{n+1} & = & 4^{y_{n}} \quad \forall n \in \mathbb{N}
        \end{cases}$$

    === "Exercice 28: "
        On considère la suite $(w_n)_{n\in \mathbb{N}}$ définie par :

        $$\forall n \in \mathbb{N} , \,  w_n = n(n - 1)(n - 2)(n - 3)(n - 4)(n - 5)(n - 6) + n$$

        1. Déterminer les cinq premiers termes de la suite.
        2. Quelle conjecture peut-on faire ?
        3. Calculer $w_7$ . Que peut-on dire de la conjecture précédente ?

    === "Exercice 29: "
        ![exercice29fig](images/exercice29fig.png){align=right width=40%}  
        1. a) Calculer  $\, \displaystyle \sum_{k=0}^{10} k$ .  
        1. b) Combien y aurait-il de boules de billard si la figure comportait $10$ rangées ?  
        2. a) Exprimer $\, \displaystyle \sum_{k=0}^{n} k$ en fonction de $n$ .  
        2. b) En arrangeant les boules de billard comme sur le dessin, combien de rangées obtient-on avec $1\ 653$ boules ?

    === "Exercice 30: "
        Pour stocker des fichiers photos numériques, on peut utiliser des algorithmes de compression pour réduire la taille du fichier. On estime qu'à chaque niveau de compression, la taille du fichier diminue de $21,4 \%$ .  
        On considère un fichier de taille initiale $T_0 = 689$ et, pour tout entier naturel non nul $n$, $T_n$ désigne la taille de ce fichier après une compression de niveau $n$.  


        1. Déterminer $T_1$ .  
        2. Exprimer $T_{n+1}$ en fonction de $T_n$ .  
        3. En déduire la nature de la suite $(T_n)_{n\in \mathbb{N}}$ .  
        4. Déterminer l'expression du terme général $T_n$ .  
        5. Déterminer la taille du fichier après une compression de niveau sept.

    === "Exercice 31: "
        ![exercice31fig](images/exercice31fig.png){align=right width=40%}
        On pose un premier grain de riz sur la première case d'un échiquier, deux grains de riz sur la deuxième case, quatre grains de riz sur la troisième case, 
        huit grains de riz sur la quatrième case, etc $\ldots$  
        
        On continue en doublant le nombre de grains de riz d'une case à la suivante.  
        Combien y a-t-il de grains de riz sur la septième case ? Sur l'échiquier tout entier ? 