def sommearithm(u,r,N):
    """
    u est le premier terme
    r est la raison
    N est le nombre de termes
    """
    S = 0
    for i in range(N):
        S = S+u
        u = u+r
    return S

assert sommearithm(1,1,100) == 5050
assert sommearithm(-10,2,50) == 1950