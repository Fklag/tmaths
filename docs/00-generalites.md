# Rappels sur les suites numériques de première générale

## Histoire des Mathématiques

??? note "Le saviez-vous ?"
    Extrait du site [wikipédia](https://fr.wikipedia.org/wiki/Suite_(mathématiques)#Fragments_d'histoire){target=_blank} :  
    Les suites numériques sont liées à la mathématique de la mesure (mesures d'un phénomène prises à intervalles
    de temps réguliers) et à l'analyse (**une suite numérique est l'équivalent discret d'une fonction numérique**). La
    notion de suite est présente dès qu'apparaissent des procédés illimités de calcul. On en trouve, par exemple, chez
    Archimède, spécialiste des procédés illimités d'approximation (séries géométriques de raison 1/4) pour des calculs
    d'aires et de volumes, ou en Égypte vers 1700 av. J.-C. et plus récemment au ier siècle apr. J.-C. dans le procédé
    d'extraction d'une racine carrée par la méthode de Héron d'Alexandrie :

    Pour extraire la racine carrée de $A$, choisir une expression arbitraire $a$ et prendre la moyenne entre $a$ et
    $\dfrac{A}{a}$ recommencer aussi loin que l'on veut le processus précédent.
    En notation moderne, cela définit la suite de nombres $(u_n)$ définit par la relation :  

    <!-- $$\left\{
    \begin{array}{rcl}
    u_0 & = & a  \\
    u_{n+1} & = & \dfrac{1}{2} \left(u_n + \dfrac{A}{u_n}\right) \quad \forall n \in \mathbb{N}\\
    \end{array}
    \right.$$ -->

    $$
    \begin{cases}
    u_0 & = & a  \\
    u_{n+1} & = & \dfrac{1}{2} \left(u_n + \dfrac{A}{u_n}\right) \quad \forall n \in \mathbb{N}\\
    \end{cases}
    $$

    On retrouve ensuite cette préoccupation plusieurs siècles plus tard (à partir du xviie siècle) avec la méthode
    des indivisibles (Cavalieri, Torricelli, Pascal, Roberval). Dans l'Encyclopédie Raisonnée de d'Alembert et Diderot
    (1751), une grande part est laissée aux suites et séries dont le principal intérêt semble être leur **convergence** :  
    Suite et série : se dit d'un ordre ou d'une progression de quantités qui croissent ou décroissent suivant quelques
    lois. Lorsque la suite va toujours en s'approchant de plus en plus de quelque quantité finie [...] on l'appelle suite
    convergente et si on la continue à l'infini, elle devient égale à cette quantité.  
    C'est ainsi que l'on voit Bernoulli, Newton, Moivre, Stirling et Wallis, s'intéresser aux suites pour approcher des
    valeurs numériques. C'est à Lagrange que l'on doit, semble-t-il, la notation indicielle. L'étude des suites ouvre
    la porte à celle des séries entières dont le but est d'approcher, non plus des nombres, mais des fonctions. Dans
    la seconde moitié du xxe siècle, le développement des calculateurs et des ordinateurs donne un second souffle à
    l'étude des suites en analyse numérique grâce à la méthode des éléments finis. On en retrouve l'usage aussi dans
    les mathématiques financières.

    Parallèlement à ces études de suites pour leur convergence, se développe un certain goût pour l'étude de la suite
    non tant pour sa convergence mais pour son terme général. C'est le cas par exemple d'un grand nombre de
    suites d'entiers comme la suite de Fibonacci, celle de Lucas ou, plus récemment, celle de Syracuse. Sont aussi
    particulièrement étudiées les suites de coefficients dans des séries entières ou les suites de nombres découvertes
    lors de dénombrements.

## Généralités sur les suites    

!!! tip "Définition"
    De manière intuitive, on appelle suite numérique une liste infinie de nombres placés dans un certain ordre.

??? faq "À titre d'exemple ▶ Pour comprendre la notation indicière"
    === "Exemple 1 : "
        $$u : 2 \, ; \, -4 \, ; \, 8 \, ; \, -16 \, ;\, 32 \, ; \, -64 \, ;\, 128 \, ;\, \ldots $$   
        On va associer à chaque nombre sa place dans la liste.  
        Si on numérote la suite à partir de $0$.  

        $$\begin{array}{lccccccccccccccc}
        u :& 2 &;& -4 &;& 8 &;& -16 &;& 32 &;& -64 &;& 128 &;& \ldots \\
        n :& 0 &;& 1 &;& 2 &;& 3 &;& 4 &;& 5 &;& 6 &;& \ldots
        \end{array}$$

        L'élément de la suite associé au rang $0$ est $2$. On note $u(0) = 2$.
        On adoptera la **notation indicielle $u_0 = 2$**. 
        
        !!! danger "Attention ! Comme la numérotation commence à $0 \,$ ▶ $u_5 = -64$ désigne le 6ème terme de la suite"
            Le terme de rang $5$ ou d'indice $5$ est le sixième terme.

    === "Exemple 2 : "
        $$v : -4 \, ; \, 2 \, ; \, \frac{2}{7} \, ; \, -5 \, ;\, -2\sqrt{3} \, ;\, \ldots $$  

        Si on numérote la suite à partir de $1$.  

        $$\begin{array}{lccccccccccccc}
        v :& -4 &;& 2 &;& \frac{2}{7} &;& -5 &;& -2\sqrt{3}  &;& \ldots \\
        n :& 1 &;& 2 &;& 3 &;& 4 &;& 5 &;& \ldots
        \end{array}$$

        L'élément de la suite associé au rang $1$ est le nombre $-4$ : $v_1 = -4$. 
        
        !!! danger "Attention ! Comme la numérotation commence à $1 \,$ ▶ $v_3 = \frac{2}{7}$ désigne le 3ème terme de la suite"
            Le terme de rang $3$ ou d'indice $3$ est le troisième terme.

!!! tip "Définition ▶ Suites numériques"
    Une suite numérique est une application de $\mathbb{N}$ (ou une partie de $\mathbb{N}$) dans $\mathbb{R}$.

    $$\begin{array}{lccccccccccccc}
        u :& \mathbb{N}  & \longrightarrow & \mathbb{R} \\
           & n & \longmapsto & u(n) = u_n
        \end{array}$$

    $u_n$ est donc l'image de l'entier $n$ par $u$.  
    À chaque rang $n$ on associe le terme $u_n$ de la suite $(u_n)$ qui lui correspond.


!!! danger "Attention ! Il ne faut pas confondre les notations suivantes :"
    $\boxed{\textcolor{red}{u_n}}$ désigne le terme de la suite de rang $n$ ou d'indice $n$.  
    $\boxed{\textcolor{red}{(u_n)}}$ désigne toute la suite, c'est-à-dire l'ensemble $\left\{u_n , n \in \mathbb{N}\right\}$.

## Relation par récurrence et relation explicite

!!! tip "Définition ▶ Suite définie par une relation de récurrence"
    Soit $n_0$ un entier naturel. Une suite $(u_n)$ est définie par récurrence si, à partir d'un certain rang $n_0$, tous les termes dont le rang est supérieur ou égal à $n_0$ s'obtiennent à partir du ou des termes précédents.

??? faq "Exemples"
    === "Exemple 1 : "
        $$
        \begin{cases}
        u_0 & = & 3 \\
        u_{n+1} & = & u_n \times 2 \quad  \text{pour tout } n \geqslant 0
        \end{cases}
        $$

    === "Exemple 2 : "
        $$
        \begin{cases}
        u_4 & = & 1 \\
        u_{n} & = & n  + \dfrac{5}{u_{n-1}} \quad \text{pour tout } n \geqslant 5
        \end{cases}
        $$

    === "Exemple 3 : "
        $$
        \begin{cases}
        u_0 & = & 5 \\
        u_1 & = & -1 \\
        u_{n+1} & = & 5 u_n^2  -3 u_{n-1} + 4 \quad \text{pour tout } n \geqslant 1
        \end{cases}
        $$   

    === "Exemple 4 : "
        Lorsqu'à partir d'un certain rang le terme suivant est dépend (est fonction) du seul terme précédent, alors il existe une fonction $f$ telle que :

        $$
        \begin{cases}
        u_{n_0} & & \text{désigne le premier terme}  \\
        u_{n+1} & = & f(u_n) \quad \forall n \in \mathbb{N}, n \geqslant n_0\\
        \end{cases}
        $$

!!! note "Développer les automatismes avec Labomep/wim's ▶ Calcul des termes d'une suite"
    === "Exercice 1: "
        [Calcul des premiers termes d'une suite définie par récurrence.](https://bibliotheque.sesamath.net/public/voir/5ce7074953c658304f477365){target=_blank}

    === "Exercice 2: "
        [Calcul des premiers termes d'une suite définie par récurrence (niveau 2).](https://bibliotheque.sesamath.net/public/voir/5ce707da1dd0a8304ed67693){target=_blank}


!!! tip "Définition ▶ Suite définie par une relation explicite"
    Soit $n_0$ un entier naturel. On dit qu'une suite $(u_n)_{n \geqslant n_0}$ est définie explicitement si son terme général est une fonction $f$ de $n$.

    $$\forall n \in \mathbb{N}, n \geqslant n_0, \quad u_n = f(n)$$


!!! note "Développer les automatismes avec Labomep/wim's ▶ Autour de la forme explicite d'une suite"
    === "Exercice 3: "
        [Écrire une expression en fonction d'une variable.](https://bibliotheque.sesamath.net/public/voir/5ce2915353c658304f477348){target=_blank}

    === "Exercice 4: "
        [Calculer des termes d'une suite définie de manière explicite.](https://bibliotheque.sesamath.net/public/voir/5ce706461dd0a8304ed67692){target=_blank}
