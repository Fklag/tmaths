<!-- # Rappels sur les suites numériques de première générale -->

<!-- ## Suites arithmétiques -->

## Exemples
??? faq "Exemples"
    === "Exemple 5 : "
        On commence à $-4$ et on passe d'un terme au suivant en ajoutant la même valeur $1$.

        $$\left\{-4 \, ; \, -3 \, ; \, -2 \, ; \, -1 \, ;\, 0 \, ; \, 1 \, ;\, 2 \, ;\, 3 \, ;\, \ldots \right\}$$   

    === "Exemple 6 : "
        On commence à $5,7$ et on passe d'un terme au suivant en ajoutant la même valeur $-0,5$.

        $$\left\{5,7 \, ; \, 5,2 \, ; \, 4,7 \, ; \, 4,2 \, ;\, 3,7 \, ; \, 3,2 \, ;\, 2,7 \, ;\, 2,2 \, ;\, \ldots \right\}$$  

## Définition par récurrence

!!! tip "Définition ▶ relation de récurrence"
    Une suite est **arithmétique** si chaque terme se déduit du précédent en lui ajoutant un nombre constant, appelé raison qui est notée $r$.  
    On commence au rang $n_0$ . Notons $a$ la valeur du premier terme.

    $$
    \begin{cases}
    u_{n_0} & = & a \\
    u_{n+1} & = & u_n + r \quad \forall n \in \mathbb{N}, n \geqslant n_0\\
    \end{cases}
    $$

    Par exemple si le premier terme est $u_0$ , on a :

    ![arithmetique](images/arithm.png)

!!! tip "Définition ▶ relation de récurrence et relation fonctionnelle associée"
    Soit $n_0$ un nombre entier.  
    La relation de récurrence d'une suite arithmétique peut s'exprimer à l'aide d'une **fonction affine**.  
    Cette fonction permet le passage d'un terme de la suite au terme suivant.

    $$
    \begin{cases}
    u_{n_0} &  &  \\
    u_{n+1} & = & f(u_n) \quad \forall n \in \mathbb{N}, n \geqslant n_0 \, \text{ où } \, f : x \mapsto x + r \\
    \end{cases}
    $$

!!! note "Exercice 5: Recherche"
    En reprenant les deux exemples précédents, déterminer le premier terme de chaque suite, la relation de récurrence, puis la fonction $f$ qui leur est associée.

    === "Exemple 5: "
        ??? bug solution "Réponse"
            - $u_0 = -4$
            - $u_{n+1} = u_{n} + 1$
            - $f : x \mapsto x + 1$


    === "Exemple 6: "
        ??? bug solution "Réponse"
            - $u_0 = 5,7$
            - $u_{n+1} = u_{n} - 0,5$
            - $f : x \mapsto x - 0,5$

!!! note "Développer les automatismes avec Labomep/wim's"
    === "Exercice 6: "
        [Identifier une suite arithmétique](https://bibliotheque.sesamath.net/public/voir/91374){target=_blank}

??? faq "À titre d'exemple ▶ Générer une suite avec GeoGebra"
    === "Exemple 7 : "
        La suite arithmétique est définie par la relation de récurrence $\begin{cases}
        u_{0} & = & -4 \\
        u_{n+1} & = & u_n + 1 \quad \forall n \in \mathbb{N} \\
        \end{cases}$

        ![exemple7](images/exemple7.png)

    === "Exemple 8 : "
        La suite arithmétique est définie par la relation de récurrence $\begin{cases}
        u_{1} & = & -5,7 \\
        u_{n+1} & = & u_n - 0,5 \quad \forall n \in \mathbb{N} \\
        \end{cases}$

        ![exemple8](images/exemple8.png)

??? faq "À titre d'exemple ▶ Génération d'une suite avec un tableur"
    === "Exemple 9 : "
        La suite arithmétique est définie par la relation de récurrence $\begin{cases}
        u_{0} & = & -4 \\
        u_{n+1} & = & u_n + 1 \quad \forall n \in \mathbb{N} \\
        \end{cases}$

        ![exemple9](images/exemple9.png){align=right width=40%}

        Les cellules A1, B1 et C1 sont étiquetées par $n$, $u_n$ et Raison.  
        On saisit en A2 la valeur $0$ et on étire vers le bas pour obtenir la première colonne.  
        Dans la cellule B2 on entre la valeur de $u_0$ c'est-à-dire le nombre $-4$.  
        Dans la cellule C2 on entre la valeur de la raison qui vaut $1$.  
        Pour obtenir les éléments de la suite $(u_n)$, on saisit dans la cellule B3, l'instruction $\boxed{=B2+C\$2}$ ,  
        ce qui indique que pour obtenir le terme suivant de la suite, on ajoute au terme précédent contenu dans B2 la raison contenue dans C2.  
        On bloque cette dernière cellule à l'aide du symbole \$ . Il suffit ensuite d'étirer la cellule B3 vers le bas pour générer la suite.

    === "Exemple 10 : "
        La suite arithmétique est définie par la relation de récurrence $\begin{cases}
        u_{1} & = & -5,7 \\
        u_{n+1} & = & u_n - 0,5 \quad \forall n \in \mathbb{N} \\
        \end{cases}$

        ![exemple10](images/exemple10.png){align=right width=40%}

        Les cellules A1, B1 et C1 sont étiquetées par $n$, $u_n$ et Raison.  
        On saisit en A2 la valeur $1$ et on étire vers le bas pour obtenir la première colonne.  
        Dans la cellule B2 on entre la valeur de $u_1$ c'est-à-dire le nombre $5,7$.  
        Dans la cellule C2 on entre la valeur de la raison qui vaut $-0,5$.  
        Pour obtenir les éléments de la suite $(u_n)$, on saisit dans la cellule B3, l'instruction $\boxed{=B2+C\$2}$ ,  
        ce qui indique que pour obtenir le terme suivant de la suite, on ajoute au terme précédent contenu dans B2 la raison contenue dans C2.  
        On bloque cette dernière cellule à l'aide du symbole \$ . Il suffit ensuite d'étirer la cellule B3 vers le bas pour générer la suite.

??? danger "Algorithme et Python ▶ Génération d'une suite arithmétique"
    On souhaite générer un nombre entier N de valeurs d'une suite arithmétique définie par récurrence.
    === "Algorithme : "
        On saisit le premier terme $u$.  
        On saisit la raison $r$.  
        On saisit $N$.  
        $L$ est une liste vide  
        Pour $i$ allant de $1$ à $N$  
        $\, \, \, \,$ On ajoute $u$ dans la liste $L$  
        $\, \, \, \,$ $u \longleftarrow u + r$  
        Fin du Pour
     
    === "Programme en `Python`: "
        ```py
        def suitearithm(u,r,N):
            """
            u est le premier terme
            r est la raison
            N est le nombre de termes
            """
            #création d'une liste vide
            L=[]
            for i in range(N):
                L.append(u)
                u=u+r
            return L

        assert suitearithm(-4,1,10) == [-4, -3, -2, -1, 0, 1, 2, 3, 4, 5]
        assert suitearithm(5.7,-0.5,8) == [5.7, 5.2, 4.7, 4.2, 3.7, 3.2, 2.7, 2.2]
        ```

## Relation explicite

!!! abstract "Propriété ▶ relation explicite"
    Relation explicite pour une suite arithmétique : soit $(u_n)_{n\geqslant 0}$ une suite arithmétique de raison $r$.  
        - $\forall n \in \mathbb{N}, u_n = u_0 + nr$ .  
        - $\forall n \in \mathbb{N}, u_n = u_1 + (n - 1)r$ .  
        - $\forall n \in \mathbb{N}, u_n = u_2 + (n - 2)r$ .  
        - etc $\ldots$

    Plus généralement, on a la formule importante suivante :

    $$\boxed{\textcolor{red}{\forall n \in \mathbb{N}, \forall p \in \mathbb{N}, u_n = u_p + (n - p)r}}$$

    On peut alors, à l'aide de la forme explicite, calculer facilement les termes de chaque rang de la suite $(u_n)$ .

??? faq "À titre d'exemple"
    === "Exemple 11 : "
        La suite arithmétique est définie par la relation de récurrence $\begin{cases}
        u_0 & = & -4 \\
        u_{n+1} & = & u_{n} + 1 \quad \forall n \in \mathbb{N}
        \end{cases}$

        Sa forme explicite est donc :

        $$
        \begin{eqnarray*}
        u_n &=& \underbrace{u_0}_{-4} + n \underbrace{r}_{1}\\
            &=& -4 + n \\
            &=& n - 4 \\
        \end{eqnarray*}
        $$

        Calculons par exemple le 11ème terme de la suite $(u_n)$. Comme le premier terme est $u_0$ , alors le le 11ème terme est $u_{10}$ .

        $$\boxed{\textcolor{red}{u_{10} = 10 - 4 = 6}}$$

    === "Exemple 12 : "
        La suite arithmétique est définie par la relation de récurrence $\begin{cases}
        u_1 & = & 5,7 \\
        u_{n+1} & = & u_{n} - 0,5 \quad \forall n \in \mathbb{N}
        \end{cases}$

        Sa forme explicite est donc :

        $$
        \begin{eqnarray*}
        u_n &=& \underbrace{u_1}_{5,7} + (n-1) \underbrace{r}_{-0,5}\\
            &=& 5,7 + (n-1)\times (-0,5) \\
            &=& 5,7 -0,5n + 0,5 \\
            &=& 6,2 -0,5n \\
            &=& -0,5n + 6,2
        \end{eqnarray*}
        $$

        Calculons par exemple le 7ème terme de la suite $(u_n)$. Comme le premier terme est $u_1$ , alors le le 7ème terme est $u_{7}$ .

        $$\boxed{\textcolor{red}{u_{7} = -0,5 \times 7 + 6,2 = 2,7}}$$

!!! note "Développer les automatismes avec Labomep/wim's ▶ Suites arithmétiques"
    === "Exercice 7: "
        [Terme général d'une suite arithmétique](https://bibliotheque.sesamath.net/public/voir/91376){target=_blank}

    === "Exercice 8: "
        [Nature et terme général d'une suite arithmétique](https://bibliotheque.sesamath.net/public/voir/91378){target=_blank}

    === "Exercice 9: "
        [Nature, terme général et calcul d'un terme d'une suite arithmétique](https://bibliotheque.sesamath.net/public/voir/91948){target=_blank}        

!!! note "Exercice: Recherche"
    === "Exercice 10: "
        Savoir déterminer la raison et le premier terme de la suite arithmétique connaissant deux de ses éléments et leur rang associé.

        Déterminer la raison et le premier terme de la suite arithmétique $(u_n)_{n\in \mathbb{N}}$ telle que $\begin{cases}
        u_5 & = & 7 \\
        u_9 & = & 19
        \end{cases}$

        ??? bug solution "Réponse"
            Pour vous aider, une vidéo d'Yvan Monka.  

            <iframe width="1128" height="846" src="https://www.youtube.com/embed/bit85U3WdLs" title="Nombre d'éléments en progression arithmétique d'un ensemble fini" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


    === "Exercice 11: "
        Savoir déterminer le nombre de termes d'une suite d'éléments d'un ensemble fini qui suivent une progression arithmétique.

        Déterminer le nombre de termes de l'ensemble suivant : $E = \left\{117, 124, 131, 138, \ldots , 551, 558, 565\right\}$ .

        ??? bug solution "Réponse"
            Pour vous aider, une vidéo 

            <iframe width="1128" height="846" src="https://www.youtube.com/embed/bit85U3WdLs" title="Nombre d'éléments en progression arithmétique d'un ensemble fini" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Représentation graphique

!!! abstract "Propriété ▶ relation explicite"
    La suite arithmétique de premier terme égal à $a$ et de raison $r$ est représentée par des points alignés sur la droite d'équation $y = rx + a$.

    Pour cette raison, on dit qu'une suite arithmétique traduit une **croissance linéaire** ou une **décroissance linéaire**, selon si la raison $r$ est positive ou négative :

    === "$r > 0$ : croissance linéaire "
        ![croilin](images/croilin.png){width=40%}

    === "$r < 0$ : décroissance linéaire "
        ![decroilin](images/decroilin.png){width=40%}

### À partir de la relation de récurrence

??? faq "À titre d'exemple"
    === "Exemple 13 :"
        Soit $(u_n)_{n\geqslant 0}$ la suite arithmétique définie par $\begin{cases}
        u_0 & = & -5 \\
        u_{n+1} & = & u_n + 3\quad \forall n \in \mathbb{N}\\
        \end{cases}$

        Représenter graphiquement la suite en vous aidant [de ce lien](https://www.geogebra.org/m/w5t5ZQPY){target=_blank} 
        
        Les éléments de la suite apparaissent sur l'axe des abscisses.

        <iframe src="https://www.geogebra.org/classic/w5t5ZQPY?embed" width="1000" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius:
        4px;" frameborder="0"></iframe>

        ??? bug solution "Réponse"
            ![exemple13](images/exemple13.png)

### À partir de la relation explicite

Il est plus facile de travailler avec une suite exprimée sous la forme explicite. **La représentation graphique d'une suite arithmétique est un alignement de points**.<!-- Voilà comment l'obtenir avec différentes applications. -->

??? faq "À titre d'exemple ▶ Avec Geogebra"
    === "Exemple 14 :"
        On saisie dans la ligne dédiée de GeoGebra l'instruction : 
        ![exemple14](images/exemple14.png)
        ![exemple14fig](images/exemple14fig.png){align=right width=40%} 

        Reprenons la suite définie dans l'exemple 9.  
        Dans cet exemple, la forme explicite du terme général est donnée par : 
        
        $$\forall n \in \mathbb{N}, u_n = n - 4$$

        Dans la ligne de saisie on écrit : $L = \text{séquence}((k, k - 4), k, 0, 9, 1)$ .  
        On obtient alors une représentation graphique des éléments de la suite avec l'ensemble des 10 points d'abscisse $k$ et d'ordonnée $u_k$ pour $k$ entier, $k \in \left[ 0~;~9\right]$.

        Mais si dans la ligne de saisie on écrit : $L = \text{séquence}(k - 4, k, 0, 9, 1)$ , on obtient la suite des éléments :  
        $\boxed{\textcolor{red}{ \text{liste1} = \left\{-4, 3, 2, 1, 0, 1, 2, 3, 4, 5\right\}}}$

## Sens de variation d'une suite arithmétique

!!! abstract "Propriété ▶ variations d'une suite arithmétique"
    Soit $(u_n)$ une suite arithmétique de raison $r$.  
    - Si $r < 0$, la suite $(u_n)$ est strictement décroissante.  
    - Si $r = 0$, la suite $(u_n)$ est constante et est toujours égale à son premier terme.  
    - Si $r > 0$, la suite $(u_n)$ est strictement croissante.

!!! note "Exercice: Recherche"
    === "Exercice 12: "
        Déterminer le sens de variation des suites définies ci-dessous.

        - $(u_n)_{n\geqslant 0}$  arithmétique de premier terme $-5$ et de raison $2,5$.  
        - $(u_n)_{n\geqslant 0}$  arithmétique de premier terme $2,5$ et de raison $-5$.  

## Somme finie des éléments d'une suite arithmétique

!!! note "Recherche"
    === "Exercice 13: "
        Calculer la somme $S = 1 + 2 + 3 + \ldots + 100$ .

        ??? bug solution "Réponse"
            Pour vous aider, une vidéo d'Yvan Monka.  

            <iframe width="1280" height="720" src="https://www.youtube.com/embed/-G3FWv5Bkzk" title="DEMONSTRATION : Somme des termes d'une suite arithmétique - Première" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    === "Exercice 14: "
        Calculer la somme $S = 1 + 2 + 3 + \ldots + n$,  pour tout entier naturel $n$ .

        ??? bug solution "Réponse"
            Pour vous aider, une vidéo d'Yvan Monka.  

            <iframe width="1280" height="720" src="https://www.youtube.com/embed/-G3FWv5Bkzk" title="DEMONSTRATION : Somme des termes d'une suite arithmétique - Première" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

!!! abstract "Propriété ▶ Somme finie"
    La somme de plusieurs termes consécutifs d'une suite arithmétique est donnée par la formule :

    $$\boxed{\textcolor{red}{S = (\text{ nombre de termes }) \times \left[ \dfrac{( 1^{\text{er}}\text{ terme } ) + (\text{ dernier terme })}{2}\right]}}$$

??? faq "À titre d'exemple"
    === "Exemple 16 :"
        Soit $(u_n)_{n\geqslant 0}$ la suite arithmétique de premier terme $-5$ et de raison $3$. On a alors :

        $u_1 = -2~;~u_2 = 1~;~u_3 = 4~;~u_4 = 7~;~u_5 = 10~;~u_6 = 13$ ; etc.

        $\forall n \in \mathbb{N},  \, u_n = u_2 + 3(n - 2) = 1 + 3n - 6 =  -5 + 3n \,$, d'où sa forme explicite :

        $\forall n \in \mathbb{N},  \, u_n = 3n - 5$ .

        Rappel sur la notation ”somme” : $\, S = u_0 + u_1 + u_2 + \ldots + u_n = \displaystyle \sum_{i=0}^{n} u_i$

        On souhaite calculer $\, \displaystyle \sum_{i=0}^{24} u_i$

        ◦ C'est la somme des 25 premiers termes ;  
        ◦ le premier terme est $u_0 = -5$ et le dernier terme est $u_{24} = -5 + 3 \times 24 = 67$ . Ainsi  

        $$\sum_{i=0}^{24} u_i = 25 \times \left(\dfrac{-5+67}{2}\right) = 775$$

        On souhaite calculer $\, \displaystyle \sum_{i=13}^{51} u_i$

        ??? bug solution "Réponse"
            ◦ C'est la somme de $39$ termes ;  
            ◦ le premier terme est $u_{13} = -5 + 3 \times 13 = 34$ et le dernier terme est $u_{51} = -5 + 3 \times 51 = 148$ . Ainsi  

            $$\sum_{i=13}^{51} u_i = 39 \times \left(\dfrac{34+148}{2}\right) = 3549$$ 

!!! note "Développer les automatismes avec Labomep/wim's ▶ Somme des termes d'une suite arithmétique"
    === "Exercice 15: "
        [Somme des termes d'une suite arithmétique](https://bibliotheque.sesamath.net/public/voir/91380){target=_blank}

??? danger "Algorithme et Python ▶ Somme finie des termes d'une suite arithmétique"
    On souhaite effectuer la somme de $N$ termes d'une suite arithmétique de raison $r$ et de premier terme $u_k$ .  
    On renseigne le premier terme $u_k$ , la raison $r$ et le nombre de termes $N$ consécutifs à sommer.
    === "Algorithme : "
        $u$ est le premier terme  
        $S \longleftarrow 0$  
        Pour $i$ allant de $0$ à $N-1$  
        $\, \, \, \,$ $S \longleftarrow S + u$  
        $\, \, \, \,$ $u \longleftarrow u + r$  
        Fin du Pour
     
    === "Programme en `Python`: "
        ```py
        def sommearithm(u,r,N):
            """
            u est le premier terme
            r est la raison
            N est le nombre de termes
            """
            S = 0
            for i in range(N):
                S = S+u
                u = u+r
            return S

        assert sommearithm(1,1,100) == 5050
        assert sommearithm(-10,2,50) == 1950
        ```
