def suitegeom(u,q,N):
    """
    u est le premier terme
    q est la raison
    N est le nombre de termes
    """
    #création d'une liste vide
    L=[]
    for i in range(N):
        L.append(u)
        u=q*u
    return L

assert suitegeom(2,3,10) == [2, 6, 18, 54, 162, 486, 1458, 4374, 13122, 39366]
assert suitegeom(5,0.5,9) == [5, 2.5, 1.25, 0.625, 0.3125, 0.15625, 0.078125, 0.0390625, 0.01953125]