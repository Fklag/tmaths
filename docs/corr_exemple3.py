def terme_rang_n(N):
    u=4
    for i in range(N):
        u=-3*i+1+u
    return u

assert terme_rang_n(0) == 4
assert terme_rang_n(1) == 5
assert terme_rang_n(2) == 3
assert terme_rang_n(3) == -2
assert terme_rang_n(50) == -3621