def f(x):
    pass

def suite_explicite(N):
    pass

assert f(3) == 75
assert suite_explicite(10) == [3, 17, 41, 75, 119, 173, 237, 311, 395, 489]