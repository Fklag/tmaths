<!-- # Rappels sur les suites numériques de première générale -->

<!-- ## Représentation graphique d'une suite -->

!!! tip "Graphique ▶ Représentation des termes d'une suite définie par récurrence"
    Soit $(u_n)$ une suite définie par récurrence. Il existe donc un premier terme de rang $n_0$ , noté $u_{n_0}$ , et une fonction $f$ , tels que : 

    $$\forall n \in \mathbb{N}, n \geqslant n_0, \quad u_{n+1} = f(u_n)$$

    À l'aide de la courbe représentative de la fonction $f$ et de la droite d'équation $y = x$, on peut représenter chaque terme de la suite définie par récurrence par un point sur l'axe des abscisses.  
    
    ??? faq "Pour en avoir une illustration"
        Observer [l'animation sous Geogebra](https://www.geogebra.org/m/mth5vNS2#material/MMdvvftc){target=_blank} dans le cas où la suite $(u_n)$ est définie par :

        $$
        \begin{cases}
        u_0 & = & 2 \\
        u_{n+1} & = & \dfrac{2u_n + 4}{2 + u_n^2} \quad \forall n \in \mathbb{N}, n \geqslant n_0\\
        \end{cases}
        $$

        <iframe src="https://www.geogebra.org/classic/MMdvvftc?embed" width="1000" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>

!!! tip "Graphique ▶ Représentation des termes d'une suite définie explicitement"
    Soit $n_0$ un entier naturel et soit $f$ une fonction définie sur $\mathbb{R}^+$ ; on définit la suite $(u_n)_{n\geqslant n_0}$ explicitement :

    $$\forall n \in \mathbb{N}, n \geqslant n_0, \quad u_{n} = f(n)$$

    À l'aide de la courbe représentative de la fonction $f$, on peut représenter chaque terme de la suite définie explicitement par un point du plan de coordonnées $\left(n~;~f(n)\right)$.

    ??? faq "Pour en avoir une illustration"
        Observer [l'animation sous Geogebra](https://www.geogebra.org/m/mth5vNS2#material/F6ZzYbxt){target=_blank} dans le cas où la suite $(u_n)_{n\in\mathbb{N}}$ est définie par son terme général $\, u_n = \dfrac{6}{n+2}$ .

        <iframe src="https://www.geogebra.org/classic/F6ZzYbxt?embed" width="1000" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>

!!! tip "Graphique ▶ Représentation graphique avec un tableur"
    La suite est définie par la relation de récurrence :

    $$
    \begin{cases}
    u_0 & = & 2 \\
    u_{n+1} & = & \dfrac{2u_n + 4}{2 + u_n^2} \quad \forall n \in \mathbb{N}, n \geqslant n_0\\
    \end{cases}
    $$

    ??? faq "Pour en avoir une illustration"
        La colonne A contient le rang $n$ de chacun des termes de la suite, la colonne $B$ les termes $u_n$ de la suite.  
        Les points représentés ont pour coordonnées $(n, u_n)$.
        Ci-joint un lien vers [la page dynamique](https://www.geogebra.org/m/mgaahhmq){target=_blank} du graphique ci-dessous.

        <iframe src="https://www.geogebra.org/classic/mgaahhmq?embed" width="1000" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>        