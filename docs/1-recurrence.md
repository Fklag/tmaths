# Suites numériques

## Raisonnement par récurrence

### Le principe de récurrence

Les suites sont souvent définies par une relation de récurrence.  
Lorsqu'on étudie une suite on s'intéresse souvent à :  
• déterminer sa forme explicite lorsqu'elle existe,  
• démontrer, le cas échéant, sa monotonie,  
• démontrer, le cas échéant, qu'elle est minorée, majorée ou/et bornée.  

Ne connaissant d'une suite que son premier terme et sa forme récurrente, c'est-à-dire la formule qui permet de passer d'un terme au suivant, pour démontrer une propriété sur la suite, il sera nécessaire et suffisant de :  

1. montrer que la propriété est vraie au moins une fois,  
2. montrer que si la propriété est vraie à un rang quelconque $k$, alors elle le sera au rang suivant $(k + 1)$.

Ces deux étapes constituent **le principe de récurrence**. Il s'articule en deux raisonnements bien distincts.

!!! bug "Axiome ▶ Principe de récurrence"
    Soit $n_0 \in \mathbb{N}$.  
    Soit $P(n)$ une propriété portant sur un entier $n$ tel que $n \geqslant n_0$ , dont on ne sait pas, a priori, si elle est vraie ou fausse.

    Pour que $P(n)$ soit vraie pour tout entier $n$ tel que $n \geqslant n_0$ , il faut et il suffit que l'on ait :  
    • **Initialisation :** $P\left(n_0\right)$ vraie ;  
    • **Hérédité :** Pour tout entier $k$ tel que $k \geqslant n_0$

    $$\text{ Si } P(k) \text{ est vraie, alors } P(k + 1) \text{ est vraie }$$

### Exercices et exemples

!!! note "Développer les automatismes avec Labomep/wim's"
    === "Exercice 1: "
        [Démontrer une égalité par récurrence.](https://bibliotheque.sesamath.net/public/voir/5e0f10761a13c53bb6ec4652){target=_blank}

!!! note "Recherche"
    === "Exercice 2: "
        Détermination d'une forme explicite  
        Soit $\left(u_n\right)_{n\geqslant 0}$ la suite définie par : 

        $$\begin{cases}
        u_0 & = & -1 \\
        u_{n+1} & = & u_{n} + 2n + 3\quad \forall n \in \mathbb{N}
        \end{cases}$$
        
        Démontrer que  $\forall n \in \mathbb{N}, \, u_n = (n+1)^2$ .

        ??? bug solution "Réponse"
            Pour une correction, suivre [la vidéo d'Yvan Monka](https://www.youtube.com/watch?v=OIUi3MG8efY){target=_blank}  

            <iframe width="1280" height="720" src="https://www.youtube.com/embed/OIUi3MG8efY" title="Démontrer par récurrence l'expression générale d'une suite - Terminale" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 

    === "Exercice 3: "
        Détermination d'une forme explicite  
        Soit $\left(a_n\right)_{n\geqslant 0}$ la suite définie par : 

        $$\begin{cases}
        a_0 & = & 4 \\
        a_{n+1} & = & 2 a_{n} - 7\quad \forall n \in \mathbb{N}
        \end{cases}$$
        
        Démontrer que  $\forall n \in \mathbb{N}, \, a_n = 7 - 3 \times 2^n$ .

    === "Exercice 4: "
        Variation et importance de l'initialisation

        1. Soit $\left(v_n\right)_{n\geqslant 0}$ la suite définie par : 

        $$\begin{cases}
        v_0 & = & 5 \\
        v_{n+1} & = & \frac{1}{5} v_{n} + 3\quad \forall n \in \mathbb{N}
        \end{cases}$$
        
        Montrer que la suite $\left(v_n\right)_{n\geqslant 0}$ est décroissante .

        1. Soit $\left(w_n\right)_{n\geqslant 0}$ la suite définie par : 

        $$\begin{cases}
        w_0 & = & -5 \\
        w_{n+1} & = & \frac{1}{5} w_{n} + 3\quad \forall n \in \mathbb{N}
        \end{cases}$$
        
        Montrer que la suite $\left(w_n\right)_{n\geqslant 0}$ est croissante .

??? faq "À titre d'exemple ▶ une inégalité importante, l'inégalité de Bernoulli"
    === "Exemple 1 : "
        Démontrer par récurrence que pour tout nombre réel $\alpha > 0$, on a :

        $$\forall n \in \mathbb{N}, \, (1 + \alpha)^n \geqslant 1 + n \alpha$$

        
        ??? bug "Démonstration"
            Soit $\alpha$ un nombre réel strictement positif. Soit $n \in \mathbb{N}$ ; on définit la propriété $P(n)$ :

            $$(1 + \alpha)^n \geqslant 1 + n \alpha$$

            On cherche à démontrer l'inégalité entre les deux membres.

            **1. Initialisation :**  
            $n = 0$ ; montrons que $P(0)$ est vraie.

            $$\left.
            \begin{array}{lr}
            \text{ D'une part : } & (1 + \alpha)^0 = 1 \\
            \text{ D'autre part : } & 1 + 0 \times \alpha = 1\\
            \end{array}\right\} \, (1 + \alpha)^0 \geqslant 1 + 0 \times \alpha , \text{ donc } P(0) \text{ est vraie } .
            $$

            **2. Hérédité :**  
            Soit un entier $k \geqslant 0$. On suppose que $P(k)$ est vraie, c'est-à-dire que $\, (1 + \alpha)^k \geqslant 1 + k \alpha$ .  
            Montrons que $P(k + 1)$ est vraie, c'est-à-dire que $\, (1 + \alpha)^{k+1} \geqslant 1 + (k+1) \alpha$ . 

            $$
            \begin{eqnarray*}
            (1 + \alpha)^k & \geqslant & 1 + k \alpha\\
            (1 + \alpha)(1 + \alpha)^k & \geqslant & (1 + k \alpha)(1 + \alpha) \quad (\text{ car } \alpha > 0, \text{ donc } 1 + \alpha > 1 > 0)\\
            (1 + \alpha)^{k+1} & \geqslant & 1 + \underbrace{k \alpha + \alpha}_{(k+1)\alpha} + k \alpha^2\\
            (1 + \alpha)^{k+1} & \geqslant & 1 + (k+1)\alpha + \underbrace{k \alpha^2}_{>0} > 1 + (k+1)\alpha \\
            (1 + \alpha)^{k+1} & \geqslant & 1 + (k+1)\alpha \\
            \end{eqnarray*}
            $$

            On a montré que si $P(k)$ est vraie, alors $P(k + 1)$ est vraie.  

            **3. Conclusion :**  
            La propriété est vraie au rang $0$ (initialisation) et elle est héréditaire, donc, d'après le principe de récurrence :

            Pour tout nombre réel $\alpha > 0$, on a :

            $$\forall n \in \mathbb{N}, \, (1 + \alpha)^n \geqslant 1 + n \alpha$$

            Pour une autre explication, suivre [la vidéo d'Yvan Monka](https://www.youtube.com/watch?v=H6XJ2tB1_fg){target=_blank}  

## Limite finie ou infinie d'une suite

!!! note "Recherche"
    On s'intéresse aux suites $\left(u_n\right)_{n\geqslant 0}$ et $\left(v_n\right)_{n\geqslant 0}$ définies ainsi :

    • $\forall n \in \mathbb{N}, \, u_n = 1,2^n -5 \qquad • \begin{cases}
    v_0 & = & 4 \\
    v_{n+1} & = & 0,6 v_{n} - 1 \quad \forall n \in \mathbb{N}
    \end{cases}$

    À l'aide de la calculatrice, représenter graphiquement ces suites ci-dessous.

    ![recherche](images/recherche.png){align=center width=60%} 

    On s'intéresse au comportement à l'infini de ces suites, c'est-à-dire quand n tend vers $+\infty$.  
    D'après ces graphiques et d'après les tableaux de valeurs, on peut conjecturer que :  
    • Si on se fixe n'importe quel nombre $A$, à partir d'un certain rang, tous les termes de la suite $\left(u_n\right)_{n\geqslant 0}$ sont supérieurs à $A$.  
    On dit que la suite $\left(u_n\right)_{n\geqslant 0}$ a pour limite $+\infty$ .  
    • Si on se fixe n'importe quel nombre $a$, à partir d'un certain rang, tous les termes de la suite $\left(v_n\right)_{n\geqslant 0}$ sont compris entre $-2,5 - a$ et $-2,5 + a$ .  
    On dit que la suite $\left(v_n\right)_{n\geqslant 0}$ pour limite $-2,5$ .

??? faq "À titre d'exemple"
    Deux animations permettent de comprendre la notion de limite de suite :

    === "1er cas :"
        Dans le cas où la suite [tend vers une limite finie](https://www.geogebra.org/m/XFtkkHTk){target=_blank} .

        <iframe src="https://www.geogebra.org/classic/XFtkkHTk?embed" width="1000" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius:
        4px;" frameborder="0"></iframe>

    === "2ème cas :"
        Dans le cas où la suite [tend vers l'infini](https://www.geogebra.org/m/pw6sZWBR){target=_blank} .

        <iframe src="https://www.geogebra.org/classic/pw6sZWBR?embed" width="1000" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius:
        4px;" frameborder="0"></iframe>

!!! tip "Définition ▶ Convergence et divergence d'une suite à l'infini"
    Soit la suite $\left(u_n\right)$. On s'intéresse à son comportement à l'infini, c'est-à-dire lorsque $n$ tend vers $+\infty$. Trois cas se présentent :

    ![convergente](images/convergente.png){align=right width=30%}

    **• 1 er cas :** la suite $\left(u_n\right)$ admet une limite finie $\ell$ à l'infini.  
    
    Si tout intervalle ouvert et centré sur $\ell$ (du type $\left]\ell - a ~;~\ell + a\right[$ avec $a > 0$) contient tous les termes de la suite à partir d'un certain rang alors on dit que la suite $\left(u_n\right)$ converge vers le réel $\ell$ . On écrit $\boxed{\textcolor{red}{\displaystyle \lim\limits_{n \to +\infty} u_n = \ell}}$

    Si une suite converge alors $\, \displaystyle \lim\limits_{n \to +\infty} u_{n+1} = \lim\limits_{n \to +\infty} u_n = \ell$ .  
    Toute suite constante converge vers la valeur de la constante.  
    Si une suite converge alors sa limite est unique.  
    Si une suite ne converge pas elle est **divergente**.


    **• 2ème cas :** la suite $\left(u_n\right)$ admet une limite infinie .  

    ![divergente](images/divergente.png){align=right width=30%}

    Si tout intervalle ouvert du type $\left]A ~;~+\infty\right[$ avec $A$ un nombre réel) contient tous les termes de la suite à partir d'un certain rang alors on dit que la suite $\left(u_n\right)$ **diverge** vers $+\infty$ . On écrit $\boxed{\textcolor{red}{\displaystyle \lim\limits_{n \to +\infty} u_n = +\infty}}$

    Il en va de même pour une limite égale à $-\infty$, si tout intervalle ouvert du type $\left]-\infty ~;~A\right[$ (avec $A$ un nombre réel) contient tous les termes de la suite à partir d'un certain rang.

    **• 3ème cas :** la suite $\left(u_n\right)$ n'admet ni de limite finie, ni de limite infinie .  

    ![divalterne](images/divalterne.png){align=right width=30%}

    On dit qu'elle n'a pas de limite. La suite est alors divergente. Par exemple la suite de terme général $(-1)^n$ .

!!! abstract "Propriété ▶ limites de référence"
    Les suites de terme général $\frac{1}{n} \, , \, \frac{1}{n^2} \, , \, \frac{1}{n^k}$ ($k$ entier naturel non nul), $\frac{1}{\sqrt{n}}$ sont convergentes et leur limite est $0$ .

    Les suites de terme général $n \, , \, n^2 \, , \, n^k$ ($k$ entier naturel non nul), $\sqrt{n}$ sont divergentes et leur limite est $+\infty$ .  

## Opérations sur les limites

On considère deux suites $\left(u_n\right)$ et $\left(v_n\right)$ dont on connaît la limite lorsque $n$ tend vers l'infini. On présente dans cette partie des résultats qui permettent d'établir les limites de $u_n + v_n$ , $u_n v_n$ et $\frac{u_n}{v_n}$ à partir de celles de $\left(u_n\right)$ et $\left(v_n\right)$ .  
Les résultats sont intuitifs ; ils ne seront pas démontrés.  
Dans certains cas, on ne peut pas prévoir la limite : on parle alors de forme indéterminée, notée **F.I.** . Pour autant, on apprendra à « lever » les indéterminations. *Dans ce qui suit, $\ell$ désigne un nombre réel*.

### Multiplication par une constante

!!! abstract "Propriété"
    Il suffit d'appliquer la règle des signes d'un produit.

    • Si $k > 0$ :

    $$
    \begin{array}{|l|*3{c|}}\hline
    \rule[-1ex]{0pt}{4ex} \displaystyle\lim_{n\to+\infty} u_n= 
    & \ell & +\infty & -\infty 
    \\\hline
    \rule[-1ex]{0pt}{4ex} \displaystyle\lim_{n\to+\infty} \left(k \times u_n\right) = 
    & k\times\ell & +\infty & -\infty  
    \\\hline
    \end{array}
    $$

    • Si $k < 0$ :

    $$
    \begin{array}{|l|*3{c|}}\hline
    \rule[-1ex]{0pt}{4ex} \displaystyle\lim_{n\to+\infty} u_n= 
    & \ell & +\infty & -\infty 
    \\\hline
    \rule[-1ex]{0pt}{4ex} \displaystyle\lim_{n\to+\infty} \left(k \times u_n\right) = 
    & k\times\ell & -\infty & +\infty  
    \\\hline
    \end{array}
    $$

### Limite d'une somme de suites

!!! abstract "Propriété"
    $$
    \begin{array}{|l|*6{c|}}\hline
    \rule[-1ex]{0pt}{4ex} \displaystyle\lim_{n\to+\infty} u_n= 
    & \ell & \ell & \ell & +\infty & -\infty & +\infty 
    \\\hline
    \rule[-1ex]{0pt}{4ex} \displaystyle\lim_{n\to+\infty} v_n= 
    & \ell' & +\infty & -\infty & +\infty & -\infty & -\infty  
    \\\hline
    \rule[-1ex]{0pt}{4ex} \displaystyle\lim_{n\to+\infty} \left(u_n+v_n\right)=
    & \ell+\ell' & +\infty & -\infty & +\infty & -\infty & 
    \textcolor{red}{\textbf{ F.I.}}
    \\\hline
    \end{array}
    $$

!!! note "Recherche ▶ $+\infty - \infty$"
    === "Exercice 3: "
        Soient les suites $\left(u_n\right)_{n\geqslant 0}$, $\left(v_n\right)_{n\geqslant 0}$  et $\left(w_n\right)_{n\geqslant 0}$ définies pour tout entier naturel $n$ par  : 

        $$u_n = n^2 \, , v_n = 1 - n^2 \, , w_n = 2 - n^2$$
        
        1. Déterminer les limites de $u_n$ , $v_n$ et de $w_n$ .  
        2. Déterminer les limites de $u_n + v_n$ et de $u_n + w_n$ .

### Limite d'un produit de suites

!!! abstract "Propriété"
    Le principe est là encore celui de la règle des signes d'un produit.
    
    $$
    \begin{array}{|l|*6{c|}}\hline
    \rule[-1ex]{0pt}{4ex} \displaystyle\lim_{n\to+\infty} u_n= 
    & \ell & \ell>0 \text{ ou } +\infty & \ell<0 \text{ ou } -\infty & \ell>0 \text{ ou } +\infty & \ell<0 \text{ ou } -\infty & 0
    \\\hline
    \rule[-1ex]{0pt}{4ex} \displaystyle\lim_{n\to+\infty} v_n= 
    & \ell' & +\infty & +\infty & -\infty & -\infty & -\infty  \text{ ou } +\infty
    \\\hline
    \rule[-1ex]{0pt}{4ex} \displaystyle\lim_{n\to+\infty} \left(u_n+v_n\right)=
    & \ell\ell' & +\infty & -\infty & -\infty & +\infty & 
    \textcolor{red}{\textbf{ F.I.}}
    \\\hline
    \end{array}
    $$

!!! note "Recherche ▶ $\infty \times 0$"
    === "Exercice 4: "
        Soient les suites $\left(u_n\right)_{n\geqslant 0}$, $\left(v_n\right)_{n\geqslant 0}$  et $\left(w_n\right)_{n\geqslant 0}$ définies pour tout entier naturel $n$ par  : 

        $$u_n = 5n^2 \, , v_n = 5n^3 \, , w_n = \frac{1}{n^2}$$
        
        1. Déterminer les limites de $u_n$ , $v_n$ et de $w_n$ .  
        2. Déterminer les limites de $u_n \times w_n$ et de $v_n \times w_n$ .

### Limite d'un quotient de suites

!!! abstract "Propriété"
    Il faut étudier l'expression pour déterminer le signe.

    $$
    \begin{array}{|l|*6{c|}}\hline
    \rule[-1ex]{0pt}{4ex} \displaystyle\lim_{n\to+\infty} u_n= 
    & \ell & \ell & \ell \neq 0 & \infty & \infty & 0
    \\\hline
    \rule[-1ex]{0pt}{4ex} \displaystyle\lim_{n\to+\infty} v_n= 
    & \ell' \neq 0 & \infty & 0 & 0 & \infty & 0
    \\\hline
    \rule[-1ex]{0pt}{4ex} \displaystyle\lim_{n\to+\infty} \left(\frac{u_n}{v_n}\right)=
    & \frac{\ell}{\ell'} & 0 & \infty & \infty & \textcolor{red}{\textbf{ F.I.}} & 
    \textcolor{red}{\textbf{ F.I.}}
    \\\hline
    \end{array}
    $$

    ??? bug solution "Retenons"
        On peut retenir facilement ces résultats en retenant les deux principes suivants :  
        • en limite, diviser par l'infini revient à multiplier par $0$ ;  
        • en limite, diviser par $0$ revient à multiplier par l'infini ;  

        puis en appliquant les résultats sur les limites d'un produit. Ainsi, le cas « $\dfrac{\infty}{\infty}$ » se ramène au cas « $\infty \times 0$ », qui est une forme indéterminée.

!!! note "Recherche"
    === "Exercice 5: "
        Déterminer les limites des suites définies pour tout entier naturel $n$ supérieur ou égal à $1$ par :

        $$u_n = n^2 + 4n + 1 \, , \,  v_n = \left(1 + \dfrac{1}{\sqrt{n}}\right)(-n + 3) \, , \,  p_n = u_n v_n \, , \,  q_n = \frac{1}{u_n}$$

### Formes indéterminées (F.I.)

On recense $4$ situations de formes indéterminées que l'on peut résumer en : « $\infty - \infty$ », « $0 \times \infty$ », « $\frac{\infty}{\infty}$ » et « $\frac{0}{0}$ ».  
Pour lever l'indétermination, il faut transformer l'écriture de l'expression pour se ramener à un des théorèmes généraux, par exemple en développant ou en factorisant.

!!! note "Recherche"
    === "Exercice 6: "
        Déterminer les limites des suites définies pour tout entier naturel $n$ supérieur ou égal à $1$ par :

        $$u_n = 2\sqrt{n}- n \, , \,  v_n = \dfrac{n^2+3n}{3n^2+4} \, , \,  p_n = \dfrac{1}{\sqrt{n}}(n+2) \, , \,  q_n = \dfrac{\frac{1}{n^2}}{\frac{2}{n}+\frac{1}{n^2}}$$

## Théorèmes sur les limites

!!! abstract "Théorème ▶ Théorème de comparaison à l'infini"
    Soit $(u_n)$ et $(v_n)$ deux suites telles qu'à partir d'un certain rang, $\, u_n \leqslant v_n$ .  
        • Si $\displaystyle\lim_{n\to+\infty}u_n = +\infty$, alors $\displaystyle\lim_{n\to+\infty}v_n = +\infty$  ;  
        • Si $\displaystyle\lim_{n\to+\infty}v_n = -\infty$, alors $\displaystyle\lim_{n\to+\infty}u_n = -\infty$  .

    ??? bug solution "Démonstration du premier point :"
        Soit $A$ un nombre réel, et $I =\left]A~;~+\infty\right[$ ; \displaystyle\lim_{n\to+\infty}u_n = +\infty$ donc il existe un entier $n_1$ tel que $\forall n \geqslant n_1 , \, u_n \in I$.

        De plus à partir d'un certain rang $n_2 , \, u_n \leqslant v_n$ , c'est-à-dire que pour tout $n \geqslant n_2 , \, u_n \leqslant v_n$ .  
        Soit $N$ un entier supérieur ou égal à $n_1$ et $n_2$ ; alors $\, \forall n \geqslant N , \, A < u_n$ et $u_n \leqslant v_n$ .  
        Donc pour tout $n \geqslant N , \, A < v_n$ donc $v_n \in I$ ; par définition : $\displaystyle\lim_{n\to+\infty}v_n = +\infty$ .

!!! note "Recherche"
    === "Observer l'animation"
        [L'animation](https://www.geogebra.org/m/eKgXZjHa){target=_blank}  pour comprendre comment on peut déterminer la limite d'une suite lorsqu'elle est encadrée par deux suites convergentes vers une même limite.

        ??? faq "Animation"
            <iframe src="https://www.geogebra.org/classic/eKgXZjHa?embed" width="1000" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius:4px;" frameborder="0"></iframe>

!!! abstract "Théorème ▶ Théorème des gendarmes (admis)"

    ![gendarmes](images/gendarmes.png){align=right width=40%}
    Soient $(u_n)$, $(v_n)$ et $(w_n)$ trois suites telles que à partir d'un certain rang, 
    
    $$\, v_n \leqslant u_n \leqslant w_n$$


    Si $(v_n)$ et $(w_n)$ convergent vers une même limite $\ell$, alors $(u_n)$ converge vers $\ell$ .

??? faq "À titre d'exemple ▶ Exercices corrigés"
    === "Exercice 7 :"
        [théorème de comparaison sur les limites](https://www.youtube.com/watch?v=iQhh46LupN4){target=_blank} .

        Déterminer $\displaystyle\lim_{n\to+\infty} \left(n^2 + (-1)^n\right)$
    
    === "Exercice 8 :"
        [théorème des gendarmes](https://www.youtube.com/watch?v=OdzYjz_vQbw){target=_blank} .

        Déterminer $\displaystyle\lim_{n\to+\infty} \left(1+ \frac{\sin(n)}{n}\right)$

!!! note "Développer les automatismes avec Labomep/wim's"
    === "Exercice 9: "
        [théorème de comparaison sur les limites](https://bibliotheque.sesamath.net/public/voir/5e0eff57c8f0043bb79307c3){target=_blank}

    === "Exercice 10: "
        [théorème de comparaison et théorème des gendarmes](https://bibliotheque.sesamath.net/public/voir/5e0f0079c8f0043bb79307c4){target=_blank}      

!!! note "Recherche"
    === "Exercice 11: "
        Déterminer les limites des suites définies pour tout entier naturel $n$ supérieur ou égal à $1$ par :

        $$u_n  = \frac{5 + (-1)^n}{n}  \quad ; \quad v_n = \frac{n+ \sin n}{n - \sin n}$$

!!! abstract "Théorème ▶ Comportement à l'infini de la suite géométrique $\left(q^n\right)$ avec $q$ réel"

    $$
    \begin{array}{|l|*4{c|}}\hline
    \rule[-1ex]{0pt}{4ex} & q \leqslant -1 & -1 < q < 1 & q = 1 & q > 1
    \\\hline
    \rule[-1ex]{0pt}{4ex} \displaystyle\lim_{n\to+\infty} q^n = 
    & \times & 0 & 1 & +\infty  
    \\\hline
    \end{array}
    $$
    
    ??? bug solution "Démonstration dans le cas où $q > 1$"
        *Prérequis :* $\forall \alpha > 0, \forall n \in \mathbb{N}, \, (1 + \alpha)^n \geqslant 1 + n \alpha$ .  

        Soit $u_n = q^n$ . Si $q > 1$, on peut poser $q = 1 + \alpha$ avec $\alpha > 0$ .  
        $\forall n \in \mathbb{N}$, on a alors $q^n = (1 + \alpha)^n$ .  

        D'après le prérequis $\, (1 + \alpha)^n \geqslant 1 + n \alpha$, donc $q^n \geqslant 1 + n \alpha$ .  
        
        Or $\displaystyle\lim_{n\to+\infty} 1 + n \alpha = +\infty$ car $\alpha > 0$, donc, d'après le théorème de comparaison, $\displaystyle\lim_{n\to+\infty} q^n = +\infty$ .

!!! note "Recherche"
    === "Exercice 12: "
        Déterminer les limites des suites définies pour tout entier naturel $n$ par :

        $$u_n  = \frac{2}{3^n}  \quad ; \quad v_n = \frac{-3}{\sqrt{2^n}} \quad ; \quad w_n = \frac{(-3)^n}{5} \quad ; \quad p_n = 2^n - 3^n$$

## Algorithme de seuil et programmation en `Python`

Lorsqu'une suite diverge vers $+\infty$, alors pour toute valeur de $A$, il existe un plus petit rang $n_0$ à partir duquel tous les termes de la suite sont supérieurs à $A$, ce qui s'écrit en langage symbolique :

$$\boxed{\textcolor{red}{\exists n_0 \in \mathbb{N} / \forall n \in \mathbb{N}, n \geqslant n_0 , \, u_n \geqslant A}}$$

La détermination du rang $n_0$ à partir duquel les termes de la suite $u_n$ dépasse le seuil $A$, peut s'obtenir à partir d'un algorithme, appelé originalement **algorithme de seuil** .

De manière analogue, lorsqu'une suite diverge vers $-\infty$, alors pour toute valeur de $A$, il existe un plus petit rang $n_0$ à partir duquel tous les termes de la suite sont inférieurs à $A$, ce qui s'écrit en langage symbolique :

$$\boxed{\textcolor{red}{\exists n_0 \in \mathbb{N} / \forall n \in \mathbb{N}, n \geqslant n_0 , \, u_n \leqslant A}}$$

??? faq "À titre d'exemple ▶ Vidéos pour programmer un algorithme de seuil"
    === "TI :"
        [Programmer sous TI](https://www.youtube.com/watch?v=Kza3KBjjsM0){target=_blank} .

    === "Casio :"
        [Programmer sous Casio](https://www.youtube.com/watch?v=9HhEeMWimNo){target=_blank} .

    === "`Python` :"
        [Programmer en `Python`](https://www.youtube.com/watch?v=vJmpzwhaka8){target=_blank} .

!!! note "Développer les automatismes avec Labomep/wim's"
    === "Exercice 13: "
        [savoir utiliser la calculatrice pour émettre des conjectures et déterminer $n_0$ .](https://bibliotheque.sesamath.net/public/voir/5e048081a726dc6681f2c4e7){target=_blank}

    === "Exercice 14: "
        [comprendre une boucle non bornée .](https://bibliotheque.sesamath.net/public/voir/5cf3f678e8933802147f4466){target=_blank}

    === "Exercice 15: "
        [savoir programmer en Python un algorithme de seuil .](https://bibliotheque.sesamath.net/public/voir/5e0481d9a726dc6681f2c4e8){target=_blank}

    === "Exercice 16: "
        [conjecturer des limites, programmer en python et déterminer un seuil à la calculatrice .](https://bibliotheque.sesamath.net/public/voir/5e0480d7bcbd3d668003f02b){target=_blank}

!!! note "Recherche"
    === "Exercice 17: "
        On considère la suite $\left(u_n\right)_{n\in \mathbb{N}}$ définie pour tout entier $n$ par $u_n = 3n^2 + 5n + 2$ .

        1. Démontrer que cette suite est strictement croissante.  
        2. Démontrer que cette suite diverge vers $+\infty$ .  
        3. On saisit une valeur $A$. Cet algorithme en pseudo-code incomplet permet de déterminer, quel que soit la valeur $A$, le plus petit rang $n_0$ à partir  duquel $\, u_{n_0} > A$ . Cette valeur est la dernière stockée dans la variable $n$ . Compléter l'algorithme en pseudo-code .  
        4. Compléter le programme en `Python` et le tester lorsque $A= 10 000$ .  
        5. Tester la fonction pour différentes valeurs de $A$ : $10 \, ; 10^2 \, ; 10^5 \, ; 10^8$  

        === "Algorithme : " 
            $n \longleftarrow \ldots$  
            $u \longleftarrow \ldots$  
            Tant que $u \ldots A$  
            $\, \, \, \,$ $n \longleftarrow n + 1$  
            $\, \, \, \,$ $u \longleftarrow \ldots$  
            Fin du Tant que

        === "Programme en `Python`: "
            ```py
            def seuil(A):
                n=0
                u=2
                while u <= A:
                    n=n+1
                    u=3*n**2+5*n+2
                return n
                
            assert seuil(10000) == 57
            ```


    === "Exercice 18: "
        On considère la suite $\left(u_n\right)_{n\in \mathbb{N}}$ définie ainsi : $\begin{cases}
        u_0 & = & 2 \\
        u_{n+1} & = & u_{n} + 2n + 1 \quad \forall n \in \mathbb{N}
        \end{cases}$ 

        1. Démontrer que cette suite est strictement croissante.  
        2. Démontrer par récurrence que $\forall n \in \mathbb{N}, u_n > n$ .  
        3. En déduire la limite de cette suite .  
        4. Ecrire un algorithme qui, pour un nombre $A$ donné, détermine le plus petit rang $n_0$ tel que $u_{n_0} > A$  .  

        === "Programme en `Python`: "
            ```py
            def seuil(A):
                u=2
                n=0
                while u .... :
                    u=...
                    n = ...
                return ...
                
            ```

## Suites majorées, minorées et bornées

!!! tip "Définition ▶ Suite minorée, majorée, bornée"
    La suite $\left(u_n\right)_{n\in \mathbb{N}}$ est majorée, s'il existe un réel $M$ supérieur à tous les termes de la suite .  
    La suite $\left(u_n\right)_{n\in \mathbb{N}}$ est majorée, $\exists M \in \mathbb{R} / \forall n\in \mathbb{N}, u_n \leqslant M$ .

    La suite $\left(u_n\right)_{n\in \mathbb{N}}$ est minorée, s'il existe un réel $m$ inférieur à tous les termes de la suite .  
    La suite $\left(u_n\right)_{n\in \mathbb{N}}$ est minorée, $\exists m \in \mathbb{R} / \forall n\in \mathbb{N}, u_n \geqslant m$ .

    La suite $\left(u_n\right)_{n\in \mathbb{N}}$ est bornée, si elle est minorée et majorée .  
    La suite $\left(u_n\right)_{n\in \mathbb{N}}$ est bornée, $\exists m \in \mathbb{R},\exists M \in \mathbb{R} / \forall n\in \mathbb{N}, m \leqslant u_n \leqslant M$ .

!!! note "Recherche"
    === "Exercice 19: "
        Soit la suite $\left(u_n\right)_{n\in \mathbb{N}^*}$ définie par $\, u_n = 3 + \frac{2}{n}$, démontrer que $\left(u_n\right)$ est bornée .

??? faq "À titre d'exemple"
    - Toute suite croissante est minorée par son premier terme : $u_n \geqslant u_{n-1} \geqslant \ldots \geqslant u_2 \geqslant u_1 \geqslant u_0$ .  
    - Toute suite décroissante est majorée par son premier terme : $u_n \leqslant u_{n-1} \leqslant \ldots \leqslant u_2 \leqslant u_1 \leqslant u_0$ .  
    - Les suites de terme général $u_n = n$, $u_n = n^2$  et  $u_n = \sqrt{n}$ sont croissantes. Elles sont donc toutes minorées par leur premier termes $u_0$ ,
      qui vaut $0$. Mais ces suites, dont on a vu précédemment qu'elles divergeaient vers $+\infty$, ne sont pas majorées ; elles ne sont pas bornées.  
    - Les suites de terme général $u_n = \frac{1}{n}$, $u_n = \frac{1}{n^2}$  et  $u_n = \frac{1}{\sqrt{n}}$ sont décroissantes. Elles sont donc toutes
      majorées par leur premier terme qui vaut $u_1 = 1$, car $n\in \mathbb{N}^*$ . Ces suites sont toutes minorées par zéro. Elles sont donc bornées par $0$ et $1$.  
    - Les suites de terme général $\sin(n)$ , $\cos(n)$ et $(-1)^n$ sont majorées par $1$ et minorées par $-1$, donc elles sont bornées .  

!!! abstract "Propriété"
    Une suite croissante non majorée a pour limite $+\infty$ .

    ??? bug solution "Démonstration"
        Soit $A$ un réel quelconque. La suite $\left(u_n\right)$ n'est pas majorée, donc il existe au moins un entier $p$ tel que : $u_p > A$.  
        La suite $\left(u_n\right)$ est croissante, donc pour tout entier $n \geqslant p,\, u_n > A$, ce qui justifie que $\displaystyle\lim_{n\to+\infty}u_n = +\infty$

!!! abstract "Propriété"
    Si une suite est croissante et admet pour limite $\ell$, alors la suite est majorée par $\ell$ .

    ??? bug solution "Démonstration"
        Pour démontrer ce résultat, on va réaliser un raisonnement par l'absurde, c'est-à-dire « prêcher le faux pour avoir le vrai ».  
        Expliquons ce qu'est précisément un raisonnement par l'absurde :  

        Rappel à propos du raisonnement par l'absurde :  
        *Pour établir qu'une proposition est vraie, on va démontrer que la proposition contraire débouche sur un résultat absurde. Autrement dit pour démontrer qu'une proposition $A$ est vraie, on va supposer que la proposition $A$ est une proposition fausse. Par une suite de déductions logiques, on arrivera à un résultat absurde (comme par exemple $1 = 0$) ; on pourra alors conclure que l'hypothèse de départ est fausse, c'est-à-dire que la proposition « $A$ est fausse » est fausse. On aura alors prouvé que la proposition $A$ est vraie.*  

        Soit $\left(u_n\right)$ une suite croissante qui admet une limite finie $\ell$.  

        Supposons que la suite ne soit pas majorée par $\ell$ .

        Alors : $\exists p \in \mathbb{N} / u_p > \ell$ .  

        Soit $I$ l'intervalle ouvert $\left]\ell - 1 ~;~ u_p \right[$ , qui contient la limite $\ell$. La suite $\left(u_n\right)$ est croissante, donc : $\forall n \geqslant p, \, u_n \geqslant u_p$ .  
        Il existe alors un intervalle ouvert $I$ contenant $\ell$ qui ne contient aucun des termes suivant $u_p$ ; c'est absurde, car la suite converge vers $\ell$ .  
        Par conséquent l'hypothèse est fausse : la suite est donc majorée par $\ell$ .

!!! abstract "Propriété ▶ Convergence d'une suite monotone (admis)"
    ![recherche](images/convmono.png){align=right width=40%} 
    • Si une suite croissante est majorée, alors elle est convergente.  
    • Si une suite décroissante est minorée, alors elle est convergente.


    ??? faq "Un exemple et un corrigé avec une Vidéo"
        === "Exercice 20 : "
        [Convergence d' une suite monotone](https://www.youtube.com/watch?v=gO-MQUlBAfo){target=_blank}

!!! note "Développer les automatismes avec Labomep/wim's"
    === "Exercice 21: "
        [Convergence d' une suite monotone](https://bibliotheque.sesamath.net/public/voir/5e5adc2124243e1364aeabf4){target=_blank}

!!! note "Recherche"
    === "Exercice 22: "
        On considère la suite $\left(u_n\right)_{n\in \mathbb{N}}$ définie ainsi : $\begin{cases}
        u_0 & = & 1 \\
        u_{n+1} & = & \frac{1}{5} u_{n} + 4\quad \forall n \in \mathbb{N}
        \end{cases}$ 

        1. Démontrer qpar récurrence que cette suite est majorée par $5$ .  
        2. Exprimer $u_{n+1} - u_n$ ; puis en utilisant le résultat précédent, montrer que la suite est croissante .  
        3. Justifier que cette suite converge vers un nombre $\ell$ .   
        4. Déterminer la valeur de la limite $\ell$ .  
        5. a) Expliquer la ligne 5 du programme en `Python`  
        ```py
        def seuil(r):
            l=...
            u=1
            n=0
            while (u < l-r) or (u > l):
                u=...
                n = n+1
            return ...
            
        ```

            b) Compléter cette fonction, qui, pour un nombre $r$ donné, renvoie le plus petit rang $n_0$ tel que $u_{n_0} \in \left[\ell - r~;~\ell\right]$ .  
            c) Tester la fonction pour différentes valeurs de $r$ : $r = 1 \, ; r=10^{-1} \, ; r=10^{-2} \, ; r=10^{-3}$  
        6.  a) Soit $\left(v_n\right)_{n\in \mathbb{N}}$ la suite définie par : $v_n = u_n - 5$ pour tout entier $n$. Montrer que $\left(v_n\right)_{n\in \mathbb{N}}$ est géométrique, donner son premier terme et sa raison.  
            b) Exprimer $v_n$ en fonction de $n$ .  
            c) En déduire une relation explicite pour $u_n$ , puis, en justifiant, retrouver la valeur de $\ell$ .  

!!! note "Développer les automatismes avec Labomep/wim's"
    === "Exercice 22: "
        [Terme général et limite d'une suite arithmético-géométrique.](https://bibliotheque.sesamath.net/public/voir/91404){target=_blank}

??? note "Recherche 1 ▶ Problème de type bac"
    On considère la suite $\left(u_n\right)$ à valeurs réelles définie par $u_0 = 1$ et, pour tout entier naturel $n$,

    $$u_{n+1} = \frac{u_n}{u_n + 8}$$

    **Partie A : Conjectures**

    Les premières valeurs de la suite $\left(u_n\right)$ ont été calculées à l'aide d'un tableur dont voici une capture d'écran :

    ![recherche](images/rbac.png){align=center width=30%}

    1. Quelle formule peut-on entrer dans la cellule $B3$ et copier vers le bas pour obtenir les valeurs des premiers termes de la suite $\left(u_n\right)$ ?  
    2. Quelle conjecture peut-on faire sur les variations de la suite $\left(u_n\right)$ ?  
    3. Quelle conjecture peut-on faire sur la limite de la suite $\left(u_n\right)$ ?  
    4. Écrire un algorithme calculant $u_{30}$ .

    ** Partie B : Étude générale**

    1. Démontrer par récurrence que, pour tout entier naturel $n,\, u_n > 0$ .  
    2. Étudier les variations de la suite $\left(u_n\right)$ .  
    3. La suite $\left(u_n\right)$ est-elle convergente ? Justifier.

    **Partie C : Recherche d'une expression du terme général**

    On définit la suite $\left(v_n\right)$ en posant, pour tout entier naturel $n, \, v_n = 1 + \dfrac{7}{u_n}

    1. Démontrer que la suite $\left(u_n\right)$ est une suite géométrique de raison $8$ dont on déterminera le premier terme.  
    2. Justifier que, pour tout entier naturel $n, \, u_n = \dfrac{7}{8^{n+1}-1}  
    3. Déterminer la limite de la suite $\left(u_n\right)$ .  

## Approfondissement

??? note "Recherche ▶ Grand Oral"
    **Étude de la convergence de la méthode de Héron.**

    Exemple de question : **pourquoi la méthode de Héron permet d'obtenir rapidement une approximation de la racine carrée d'un nombre positif $A$ ?**

    *Situation déclenchante :*  
    Sujet riche qui mêle histoire, culture, et qui fait intervenir de nombreux éléments du programme à tous les niveaux ; le sujet permet d'aborder différents types de démonstration : raisonnement par l'absurde, récurrence, raisonnement déductif,...

    La situation déclenchante est liée à la richesse du problème historique qui mêle la géométrie et l'analyse et débouche sur des programmes en Python.  
    De nombreux approfondissements sont possibles.  
    • Comprendre que les nombres réels, tels que nous les connaissons, sont issus d'un processus lent.  
    • Évoquer la découverte de l'irrationnalité de certains nombres comme la longueur de la diagonale d'un carré de côté $1$ (raisonnement par l'absurde).  
    • Expliquer d'où vient la formule de récurrence de la suite.  
    • Établir la preuve de la convergence de la méthode de Héron.  
    • Programmer en `Python` pour approcher, à une précision souhaitée, la racine carrée d'un nombre positif.  
    • Approfondir en parlant de la vitesse de convergence de la suite. La comparer avec une autre méthode : dichotomie par exemple.  
    • Approfondir en montrant que la méthode de Héron est un cas particulier de la méthode de Newton et de la méthode de la tangente.  

    L'élève peut avoir rencontré ce problème à différents niveaux de sa scolarité. Il peut alors justifier pourquoi ce thème l'a intéressé(e).

    ![recherche](images/babylone.png){align=center width=30%}

    Si on attribue à Héron l'Ancien (ier siècle av. J.-C), encore appelé Héron d'Alexandrie en référence à la ville grecque, une formule récurrente d'approximation de la racine carrée d'un nombre $A$, la méthode semblait connu des Babyloniens, plusieurs siècles auparavant ; c'est pourquoi l'algorithme de Héron s'appelle aussi l'algorithme de Babylone.

    **I. L'algorithme de Babylone et la suite $\begin{cases}
        u_0 & = & a \\
        u_{n+1} & = & \frac{1}{2} \left(u_{n} + \frac{A}{u_n}\right) \quad \forall n \in \mathbb{N}
        \end{cases}$**

    On souhaite construire un carré d'aire $a \text{cm}^2$ ce qui revient, avec les notations d'aujourd'hui, de construire un segment de longueur $\sqrt{a}$ cm.   
    
    Tout comme les Grecs, nous allons effectuer une construction géométrique pas à pas.

    1. **Etape 0.**  
    On considère un rectangle de longueur $u_0$ et d'aire $A$.  
    Exprimer sa largeur $l_0$ en fonction de $A$ et $u_0$ .

    2. **Etape 1.**
    Soit un nouveau rectangle d'aire $A$ dont la longueur $u_1$ est la moyenne arithmétique de $u_0$ et $\ell_0$ .  
    Exprimer $u_1$ en fonction de $A$ et $u_0$ .

    3. **Etape 2.**  
    De manière analogue, on considère un nouveau rectangle d'aire $A$ dont la longueur $u_2$ est la moyenne arithmétique de la longueur et la largeur du précédent rectangle. Exprimer $u_2$ en fonction de $A$ et $u_1$ .

    4. **Etape $(n+1)$.**  
    En réitérant le raisonnement précédent, exprimer la longueur $u_{n+1} en fonction de $A$ et $u_n$ .  

    **La répétition des différentes étapes est le fondement de l'algorithme de Héron (de Babylone).**

    **II. Étude de la la suite $\left(u_n\right)$  définie par $\begin{cases}
        u_0 & = & a \\
        u_{n+1} & = & \frac{1}{2} \left(u_{n} + \frac{A}{u_n}\right) \quad \forall n \in \mathbb{N}
        \end{cases}$**

    On choisit le réel $a$ de manière à ce que $a \geqslant A$ .  
    On pose $f$ la fonction définie sur $\left[\sqrt{A}~;~+\infty\right[$ par $f(x)= \frac{1}{2}\left(x+\frac{A}{x}\right)$ .  

    1. Démontrer que $f$ est strictement croissante et continue (*Nécessaire d'avoir vu le chapitre sur la continuité*) sur $\left[\sqrt{A}~;~+\infty\right[$ .  
    2. Démontrer par récurrence que, pour tout entier naturel $n,\, u_n \geqslant \sqrt{A}$  autrement dit que la suite $\left(u_n\right)$ est minorée par $\sqrt{A}$ .  
    3. Démontrer que la suite $\left(u_n\right)$ est décroissante.  
    4. Démontrer que la suite est convergente.  
    5. Déterminer sa limite.  
    6. Prolongement possible : la convergence quadratique de la suite $\left(u_n\right)$. Comparaison de la vitesse de convergence avec, par exemple, la méthode dichotomie (*voir chapitre sur la continuité*).  

    $$u_{n+1} - \sqrt{A} = \frac{1}{2} \left(u_{n} + \frac{A}{u_n}\right) = \frac{1}{2u_n}\left(u_n^2 - 2u_n \sqrt{A} + A\right) = \frac{1}{2u_n}\left(u_n - \sqrt{A}\right)^2$$

    Si $A \geqslant 1, \, \forall n \in \mathbb{N}, \, u_n \geqslant 1$ et $u_{n+1} - \sqrt{A} \leqslant \frac{1}{2u_n}\left(u_n - \sqrt{A}\right)^2$  
    Ainsi, si à l'étape $n$, $\, u_n - \sqrt{A} \leqslant 10^{-n}$ alors à l'étape suivante $\, $u_{n+1} - \sqrt{A} \leqslant 10^{-2n}$  
    7. Lien avec la méthode de Newton

    **III. Application, algorithme et programmation en `Python` .**

    1. Déterminer les valeurs exactes de $u_1 , u_2 , u_3$ et $u_4$ dans le cas où $u_0 = 4$ et $A = 5$, puis en donner une valeur arrondie à $10^{-6}$ près .  
    2. Construire les rectangles de longueurs $u_0$ , $u_1 , u_2 , u_3$ puis $u_4$ .  
    3. Compléter la fonction suivante qui renvoie $N$ termes de la suite $\left(u_n\right)$ .  
    ```py
        def babylone(A,a,N):
            l=[]
            u=a
            for i in range(N):
                l.append(u)
                u=...
            return ...
      
    ```

    4. Donner alors une valeur arrondie de $\sqrt{5}$ à $10^{-5}$ près. Construire un carré de côté $u_4$ . Quelle est son aire ? conclure .  
    5. Tester la fonction suivante en saisissant par exemple l'instruction « trace(2,2,8) » ! Que fait-il ?  
    La fonction « babylone » est appelée par la fonction « trace ».  
    Ces programmes se retrouvent sur la plateforme [Jupyter](https://mybinder.org/v2/gh/PHILMAG1/MAGPHI/8ea485adc577bb65ddd0bf199957e97baddb0550?filepath=Chapitre%20suite%20progr%20Python-Copy1(2).ipynb){target=_blank}

    ```py
    import turtle
    from turtle import*

    def trace(A,a,N):
        #A : nombre dont on cherche la racine carrée
        #a : valeur approchée par excès de la racine carrée
        #N : nombre de rectangles à tracer
        up()
        goto(-450,-350)
        down()
        setheading(-90)
        l=babylone(A,a,N)
        for i in range(N):
            if i%4==0:
                turtle.pencolor("orange")
            elif i%4==1:
                turtle.pencolor("blue")
            elif i%4==2:
                turtle.pencolor("red")
            elif i%4==3:
                turtle.pencolor("green")
            for j in range(2):
                left(90)
                forward(l[i]*700/a)
                left(90)
                forward((A*700/a)/l[i])
        done()
    ```

    *Quelques éléments bibliographiques qui peuvent aider :*

    [https://www.irem.univ-mrs.fr/IMG/pdf/BabyloneV2-2.pdf](https://www.irem.univ-mrs.fr/IMG/pdf/BabyloneV2-2.pdf){target=_blank}  
    [https://fr.wikipedia.org/wiki/M%C3%A9thode_de_H%C3%A9ron](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_H%C3%A9ron){target=_blank}  
    [http://mediamaths.over-blog.com/article-web-et-mathematiques-babyloniennes-45144085.html](http://mediamaths.over-blog.com/article-web-et-mathematiques-babyloniennes-45144085.html){target=_blank}  

    Mais aussi et surtout ! De André Seguin IREM de la Réunion  
    [https://maths.discip.ac-caen.fr/spip.php?article466](https://maths.discip.ac-caen.fr/spip.php?article466){target=_blank}  
    [https://irem.univ-reunion.fr/IMG/pdf/methode_heron.pdf](https://irem.univ-reunion.fr/IMG/pdf/methode_heron.pdf){target=_blank}  

    Et d'Alain Busser  
    [https://irem.univ-reunion.fr/spip.php?article531](https://irem.univ-reunion.fr/spip.php?article531){target=_blank}  