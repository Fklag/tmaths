site_name: "Terminale Maths - Suites numériques"
site_url: https://fklag.gitlab.io/tmaths
repo_url: https://gitlab.com/Fklag/tmaths
edit_uri: tree/main/docs/

docs_dir: docs

nav:
  - "🏡 Accueil": index.md
  #- ... | regex=^(?:(?!_REM.md).)*$
  - "Rappels sur les suites numériques de première générale": 
    - Généralités sur les suites : 00-generalites.md
    - Variation d'une suite : 01-variations.md
    - Représentation graphique d'une suite : 02-representation.md
    - Suites arithmétiques : 03-suites_arithmetiques.md
    - Suites géométriques : 04-suites_geometriques.md
  - "Suites numériques": 
    - Raisonnement par récurrence : 1-recurrence.md


theme:
    favicon: images/favicon.ico
    icon:
      logo: material/application-variable

    custom_dir: my_theme_customizations/

    name: material
    font: false                     # RGPD ; pas de fonte Google
    language: fr                    # français
    palette:                        # Palettes de couleurs jour/nuit
      - media: "(prefers-color-scheme: light)"
        scheme: default
        primary: teal # indigo
        accent: indigo
        toggle:
            icon: material/weather-sunny
            name: Passer au mode nuit
      - media: "(prefers-color-scheme: dark)"
        scheme: slate
        primary: blue
        accent: blue
        toggle:
            icon: material/weather-night
            name: Passer au mode jour
    features:
        - navigation.instant
        - navigation.tabs
        - navigation.top
        - toc.integrate
        - header.autohide


markdown_extensions:
    - meta
    - abbr

    - def_list                      # Les listes de définition.
    - attr_list                     # Un peu de CSS et des attributs HTML.
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - admonition                    # Blocs colorés  !!! info "ma remarque"
    - pymdownx.details              #   qui peuvent se plier/déplier.
    - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.highlight:           # Coloration syntaxique du code
        auto_title: true
        auto_title_map:
            "Python": "🐍 Script Python"
            "Python Console Session": "🐍 Console Python"
            "Text Only": "📋 Texte"
            "E-mail": "📥 Entrée"
            "Text Output": "📤 Sortie"
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
        custom_checkbox:    false   #   avec cases d'origine
        clickable_checkbox: true    #   et cliquables.
    - pymdownx.tabbed:              # Volets glissants.  === "Mon volet"
        alternate_style: true 
    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.emoji:               # Émojis  :boom:
        emoji_index:     !!python/name:materialx.emoji.twemoji
        emoji_generator: !!python/name:materialx.emoji.to_svg


    - pymdownx.superfences:
        custom_fences:
          - name: mermaid
            class: mermaid
            format: !!python/name:pymdownx.superfences.fence_code_format



    - pymdownx.arithmatex:
        generic: true
    - toc:
        permalink: ⚓︎
        toc_depth: 3

extra:
  social:
    - icon: fontawesome/solid/paper-plane
      link: mailto:lycee.lagrave@free.fr
      name: Mail

    #- icon: fontawesome/brands/gitlab
    #  link: https://www.gitlab.com/Fklag/tnsi_programmation/
    #  name: Dépôt git


    - icon: fontawesome/solid/school #mattermost/mattermost
      link: http://lycee.lagrave.free.fr
      name: F. Lagrave

    - icon: fontawesome/brands/discourse  #fontawesome/solid/university
      link: https://www.atrium-sud.fr
      name: Lycée Beaussier

    #- icon: fontawesome/brands/discourse
    #  link: https://mooc-forums.inria.fr/moocnsi/
    #  name: Le forum des enseignants de NSI

  site_url: https://fklag.gitlab.io/tmaths


plugins:
  - search
  - exclude-search:           # exclusion de pages de la recherche
      exclude:
        - N*/*/*/*.md         # les REM
        - N*/*/*/*.py         # Les corrigés
        - xtra/*.md           # inutile de chercher cela
  - awesome-pages:
        collapse_single_pages: true
  - macros

extra_javascript:
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
  - https://unpkg.com/mermaid@8.9.2/dist/mermaid.min.js
  - https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/highlight.min.js
  # - javascripts/config.js
  # - javascripts/mermaid.js
  #- https://cdnjs.cloudflare.com/ajax/libs/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML
  # - xtra/config.js # bug \textcolor{red}
  - javascripts/config.js # - MathJax
  - xtra/interpreter.js

extra_css:
  - xtra/pyoditeur.css
  - xtra/ajustements.css                      # ajustements
  - https://highlightjs.org/static/demo/styles/base16/papercolor-light.css #-night-bright.css
